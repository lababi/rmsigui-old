library(shiny)
library(MALDIquantForeign)
library(MALDIquant)
library(e1071)
library(lattice)
library(latticeExtra)
library(colorspace)
library(rmsi)
library(EBImage)
library(RColorBrewer)
library(mixOmics)

# Allow uploading files up to 1 Gb
options(shiny.maxRequestSize = 1024**8)

# 
shinyServer(function(input, output, session) {
  
  # precalculate/cache gcode
  gcode <- reactive( {
    if (is.null(input$gx) ||
        is.null(input$gy) ||
        is.null(input$gres) ||
        is.null(input$gtime)) {
      return(NULL)
    }
    return(rmsi:::.gcode(input$gx, input$gy, input$gres, input$gtime))
  })
  
  # Gcode panel
  output$gcodeplot <- renderPlot({
    xy <- expand.grid(x = seq(0, input$gx, by = input$gres),
                      y = seq(0, input$gy, by = input$gres))
    plot(xy, asp=1)
    
    if (!is.null(gcode()) && isTRUE(input$garrows)) {
      p <- rmsi:::.parsegcode(gcode()[2,], relative = FALSE)[, -3]
      arrows(c(0, p[-nrow(p), "x"]), c(0, p[-nrow(p), "y"]),
             p[, "x"], p[, "y"], length=0.1)
    }
  })
  
  # Gcode panel output messages
  output$estimateSamplingTime <- renderText({
    sprintf("Approximate sampling time: %.1f s", ncol(gcode()) * input$gtime)
  })
  output$spotsSpectra <- renderText({
    paste0("Sampling spots/spectra: ", ncol(gcode()))
  })
  
  # Gcode panel Gcode-download
  output$downloadgcode <- downloadHandler(
    filename = function() {
      paste0("X_", input$gx, "-Y_", input$gy,
             "-RES_", input$gres, "-WAIT_", input$gtime, ".gcode")
    },
    content = function(file) {
      rmsi::gcode(file, input$gx, input$gy, input$gres, input$gtime)
    }
  )
  
  # Gcode panel csv-download
  output$downloadcsv <- downloadHandler(
    filename = function() {
      paste0("X_", input$gx, "Y_", input$gy,
             "RES_", input$gres, "WAIT_", input$gtime, ".csv")
    },
    content = function(file) {
      rmsi::positioncsv(file, input$gx, input$gy, input$gres)
    }
  )
  
  #  mzML loading
  observeEvent(input$goloadmzml, {
    
    # Load mzML spectra
    filenameMZ <- file.path(dirname(as.character(input$mzMLfile$datapath)), input$mzMLfile$name)
    file.rename(as.character(input$mzMLfile$datapath), filenameMZ)
    # spectra <- importMzMl(filenameMZ) #, centroided = TRUE)
    
    # mzMLfile <- input$mzMLfile
    if (is.null(filenameMZ)) {
      return(NULL)
    }
    print(filenameMZ)
    spectra <- importMzMl(filenameMZ)
    
    # Processing spectra?
    print("Pre-processing spectra")
    
    # Set general parameters
    halfWindowSize <- 20
    tolerance <- 0.1
    SNR <- 3

    # Smooth
    print("Smoothing")
    spectra <- smoothIntensity(spectra, halfWindowSize = halfWindowSize/2)
    
    # Remove baseline
    print("Removing baseline")
    spectra <- removeBaseline(spectra)
    
    # Align
    # print("Aligning")
    # spectra <- alignSpectra(spectra, halfWindowSize = halfWindowSize, SNR = SNR, tolerance = tolerance)
    
    
    
    # Get spetra info
    print("Spectra info")
    elementsSpetrum <- length(spectra)
    mzRange <- range(unlist(lapply(spectra, MALDIquant::mass)))
    minmzRange <- floor(mzRange[1])
    maxmzRange <- ceiling(mzRange[2])
    maxint <- max(unlist(lapply(spectra, intensity)))
    
    # Load csv coordinates
    print("Loading coordinates")
    filenameCSV <- file.path(dirname(input$csvfile$datapath), input$csvfile$name)
    file.rename(input$csvfile$datapath, filenameCSV)
    coordsTab <- read.table(filenameCSV, sep = ",")
    
    # Output info
    output$mzMLstatus <- renderText("Summary for loaded imzML and csv files:")
    output$mzMLinfo <- renderTable({
      categories <- c("File", "Number of spectrum", "m/z range", "Max intensity", "Number of localized samples")
      values <- c(toString(basename(filenameMZ)),
                  elementsSpetrum,
                  paste(minmzRange, maxmzRange, sep="-"),
                  maxint,
                  nrow(coordsTab))
      data.frame(categories, values, stringsAsFactors = FALSE)
    })
    
    
    
    # Get chromatogram
    # chrom <- sapply(spectra, function(x) sum(intensity(x)))
    # chrom <- chrom/max(chrom)
    # plot(chrom, type = "l", xlab = "Scan", ylab = "Relative Abundance")
    # grid()
    
    
    
    
    # Select spectra (subset/nearest)
    print("Select spectra")
    # ms <- openMSfile(filenameMZ)
    # hd <- header(ms)
    # spectra <- spectra[1:length(spectra)]
    
    
    
    
    spectra <- spectra#[seq(1, round(nrow(coordsTab)*0.9))]
    if (length(spectra) >= nrow(coordsTab) & length(spectra) < round(1.2*nrow(coordsTab))) {
      print("Option 1")
      spectra <- spectra[1:nrow(coordsTab)]
    }
    if (length(spectra) >= nrow(coordsTab) & length(spectra) >= round(1.2*nrow(coordsTab))) {
      print("Option 2")
      # spectra <- c(spectra, rep(spectra[length(spectra)], round(nrow(coordsTab)*2.2)-length(spectra)))
      spectra <- spectra[ceiling(seq(0, length(spectra), by = length(spectra)/nrow(coordsTab)))]
    }
    if (length(spectra) < nrow(coordsTab)) {
      print("Option 3")
      spectra <- spectra[ceiling(seq(0, length(spectra), by = length(spectra)/nrow(coordsTab)))]
    }
    spectra <- spectra[1:nrow(coordsTab)]
    
    
    
    
    # Export spectra to imzml and ibd files to tmp dir
    print("Exporting")
    outfile <- tempfile(fileext = '.imzml')
    exportImzMl(x = spectra, path = outfile,  #pixelSize = c(1000, 1000),
                coordinates = as.matrix(coordsTab), force = TRUE)
    
    # Zip imzml and ibd files?
    zip(zipfile = sub(".imzml", ".zip", outfile), 
        files = c(sub(".imzml", ".imzml", outfile), sub(".imzml", ".ibd", outfile)), 
        flags = "-r1X")
    
    # imzml generation status
    output$imzMLstatus <- renderText("mzML and csv files loaded and imzML created")
    
    # Download imzml file
    output$downloadimzML <- downloadHandler(
      filename = function() {
        "spectra_imzML.zip"
      },
      content = function(file) {
        file.rename(sub(".imzml", ".zip", outfile), file)
      }, contentType = "zip")
    outputOptions(output, 'downloadimzML', suspendWhenHidden = FALSE)
    
  })
  
  
  
  # imzML processing 
  observeEvent(input$goloadimzml, {
    
    # Load imzML file 
    filename <- file.path(dirname(as.character(input$imzMLfile$datapath)), input$imzMLfile$name)
    file.rename(as.character(input$imzMLfile$datapath), filename)
    imagespectra <- importImzMl(filename, centroided = TRUE)
    elementsimzML <- length(imagespectra)
    
    # Load ambient noise mzML file
    # filenameNoise <- file.path(dirname(as.character(input$noiseMzMLfile$datapath)), input$noiseMzMLfile$name)
    # file.rename(as.character(input$noiseMzMLfile$datapath), filenameNoise)
    # noisespectra <- importMzMl(filenameNoise, centroided = TRUE)
    
    # Gather labeled sample and ambient spectra
    # names(imagespectra) <- rep("sample", length(imagespectra))
    # names(noisespectra) <- rep("noise", length(noisespectra))
    # imagespectra <- c(imagespectra, noisespectra)
    
    # Get picture
    filenamePicture = file.path(dirname(input$pictureFile$datapath), input$pictureFile$name)
    file.rename(input$pictureFile$datapath, filenamePicture)
    img <- readImage(filenamePicture)
    
    # Basic mz slice information
    imagepos <- coordinates(imagespectra)
    minx <- min(imagepos[1,])
    maxx <- max(imagepos[1,])
    miny <- min(imagepos[2,])
    maxy <- max(imagepos[2,])
    
    xrange <- maxx - minx + 1
    yrange <- maxy - miny + 1
    outxrange <- xrange - 1
    outyrange <- yrange - 1
    outxyrange <- paste(outxrange, sep="x", outyrange)
    
    intmatrix <- matrix(0, yrange, xrange)
    colnames(intmatrix) <- seq(minx,maxx)
    rownames(intmatrix) <- seq(miny,maxy)
    
    mzRange <- range(unlist(lapply(imagespectra, mass)))
    minmzRange <- floor(mzRange[1])
    maxmzRange <- ceiling(mzRange[2])
    
    # Update m/z range for input fields
    updateNumericInput(session, "mastermin", value = minmzRange)
    updateNumericInput(session, "mastermax", value = maxmzRange)
    outmzRange <- toString(paste(minmzRange, sep = "-", maxmzRange))
    maxint <- max(unlist(lapply(imagespectra, intensity)))
    
    # Output loaded imzML info
    output$imzMLstatus <- renderText("Summary for loaded imzML file:")
    output$imzMLinfo <- renderTable({
      categories <- c("file", "number of spectra", "m/z range", "image dimensions", "sub-sampling value", "max intensity")
      values <- c(toString(basename(filename)),
                  elementsimzML,
                  paste(minmzRange, maxmzRange, sep="-"),
                  outxyrange,
                  input$topNval,
                  maxint)
      data.frame(categories, values, stringsAsFactors = FALSE)
    })
    
    # Show picture
    output$picture = renderPlot({
      display(img, method = "raster")
    })
    
    # Picture status
    output$pictureStatus <- renderTable({
      categories <- c("Number of pixels (millions)", "Pixels in x", "Pixels in y")
      values <- c(toString(round(nrow(img)*ncol(img)/1e6, 2)),
                  dim(img)[1],
                  dim(img)[2])
      data.frame(categories, values, stringsAsFactors = FALSE)
    })
    
    # Setup image spectra
    print("Setting up data")
    # imagespectra <- smoothIntensity(imagespectra)
    # imagespectra <- removeBaseline(imagespectra)
    imagespectra <- lapply(imagespectra, function(x) {
      MALDIquant::intensity(x) <- MALDIquant::intensity(x)/sum(MALDIquant::intensity(x)) * 1e6
      return(x)
    })
    
    # Downsampling
    # imagespectraPeaks <- detectPeaks(imagespectra, SNR = 10)
    imagespectraTop <- topN(imagespectra, n = input$topNval)
    imagespectra <- imagespectraTop
    
    
    # Get density and master spectrum
    mzdensity <- density(unlist(lapply(imagespectra, mass)), bw = 0.01)
    masterspectrum <- masterSpectrum(imagespectra, method = "sum")
    
    # Get peaks of interest
    roi <- detectPeaks(masterspectrum, SNR = input$snratio,
                       halfWindowSize = 3)
    
    # Generate density plot
    output$densityplot <- renderPlot({
      plot(mzdensity, xlim = c(input$mastermin, input$mastermax),
           xlab = "m/z", ylab = "density", main = "m/z density distribution")
      # polygon(mzdensity, col = "grey")
    })
    
    # Generate master spectra
    output$masterspec <- renderPlot({
      plot(masterspectrum, xlim = c(input$mastermin, input$mastermax),
           xlab="m/z")
      peaks <- detectPeaks(masterspectrum, SNR = input$snratio)
      output$peaklist <- renderPrint({peaks})
      points(peaks, pch = 18)
    })
    
    # create peak-Zoom spectra
    output$peakzoom <- renderPlot({
      minzoom <- input$mzcenter - input$peakzoomtol
      maxzoom <- input$mzcenter + input$peakzoomtol
      peaks <- detectPeaks(masterspectrum, SNR = input$zoomsnratio)
      maxintpeaks <- max(peaks@intensity)
      
      setylim <- maxintpeaks/input$peakzoomint
      
      plot(masterspectrum, main = paste("S/N", input$zoomsnratio),
           xlim = c(minzoom, maxzoom), ylim=c(0, setylim), xlab="m/z")
      output$peaklist <- renderPrint({peaks})
      points(peaks, pch = 18)
      abline(v = input$mzcenter)
    })
    
    
    # Get MS slices
    slices <- reactive({
      
      # Get ms slices  
      msSlices <- msiSlices(imagespectra,
                            center = c(input$mzcenter, MALDIquant::mass(roi)),
                            # center = c(mass(roi)),
                            tolerance = input$mztolerance, method = "sum", adjust = TRUE)
      
      # Get wieghts
      # featureTab <- data.frame(apply(msSlices, 3, unlist), stringsAsFactors = FALSE)
      # colnames(featureTab) <-  c(input$mzcenter, mass(roi))
      # nNoise <- input$nNoise
      # featureTab$type <- c(rep("noise", nNoise), 
      #                      rep("sample", nrow(featureTab)-(nNoise*2)), 
      #                      rep("noise", nNoise))
      # nb <- naiveBayes(type ~ ., data = featureTab)
      # nbWeights <- sapply(nb$tables, function(x) (x[, 1]/sum(x[, 1]))["sample"])
      # for (slice in dim(msSlices)[3]) {
      #   msSlices[,, slice] <- msSlices[,, slice] * nbWeights[slice]
      # }
      
      #
      msSlices
    })
    
    
    # Generate m/z intensity map
    output$mzimage <- renderPlot({
      slice <- slices()[,, 1]
      levelplot((slice),
                main = paste0("m/z ", input$mzcenter, " +/- ", input$mztolerance),
                xlab = "x/ pixel", ylab = "y/ pixel",
                scales = list(draw = FALSE), contour = TRUE, pretty = TRUE,
                col.regions = heat_hcl(100, h = c(0, 250), c. = c(30, 150),
                                       l = c(100, 25), power = c(1/5, 3),
                                       gamma = NULL, fixup = TRUE, alpha = 1))
    })
    
    # Process sample picture and get mask
    maxSize <- 256
    imgReg <- EBImage::resize(img, w = ifelse(dim(img)[1] > dim(img)[2], maxSize, round(dim(img)[1]/dim(img)[2]*maxSize)),
                              antialias = TRUE, filter = "none")
    nmask <- imgReg
    nmask <- normalize(nmask)
    nmask <- ifelse(nmask > 0.8, 1, 0)
    nmask <- dilate(nmask, makeBrush(3, shape = "Gaussian"))
    nmask <- fillHull(nmask)
    
    # Image analysis
    images <- reactive({
      
      # Re-size images
      mzSlice <- slices()[,, 1]
      # mzSlice <- EBImage::resize(mzSlice, w = dim(imgReg)[1], h = dim(imgReg)[2])
      imgReg <- EBImage::resize(imgReg, w = dim(mzSlice)[1], h = dim(mzSlice)[2], output.dim = dim(mzSlice))
      nmask <- EBImage::resize(nmask, w = dim(mzSlice)[1], h = dim(mzSlice)[2], output.dim = dim(mzSlice))
      
      imgReg <- EBImage::translate(x = imgReg, v = c(input$`x-translation`, -input$`y-translation`), output.dim = dim(mzSlice))
      imgReg <- EBImage::resize(imgReg, w = dim(imgReg)[1]*input$`x-resize`, 
                                h = dim(imgReg)[2]*input$`y-resize`, output.dim = dim(mzSlice))
      imgReg <- EBImage::rotate(x = imgReg, angle = -input$`right-rotation`, output.dim = dim(mzSlice))
      
      nmask <- EBImage::translate(x = nmask, v = c(input$`x-translation`, -input$`y-translation`), output.dim = dim(mzSlice))
      nmask <- EBImage::resize(nmask, w = dim(nmask)[1]*input$`x-resize`, 
                               h = dim(nmask)[2]*input$`y-resize`, output.dim = dim(mzSlice))
      nmask <- EBImage::rotate(x = nmask, angle = -input$`right-rotation`, output.dim = dim(mzSlice))
      nmask <- nmask[,,1]
      nmask <- ifelse(nmask > 0.5, 1, 0)
      
      # 
      list("imgReg" = imgReg, "nmask" = nmask, "mzSlice" = mzSlice)
    })
    
    # Perform ROI univariate, get data ro ROI multi-variate
    maLatVar <- reactive({
      nmask <- images()[["nmask"]]
      maTab <- data.frame(row.names = 1:dim(slices())[3])
      maTab$`m/z` <- attributes(slices())$center
      latVar <- list()
      for (i in 1:dim(slices())[3]) {
        mzSlice <- slices()[,, i]
        mzSlice <- EBImage::resize( mzSlice, w = dim(nmask)[1], h = dim(nmask)[2])
        ROI <- nmask * mzSlice
        Other <- abs(nmask-1) * mzSlice
        x <- log10(c(mean(ROI, na.rm = TRUE) + mean(Other, na.rm = TRUE)))
        y <- log2(c(mean(ROI, na.rm = TRUE) / mean(Other, na.rm = TRUE)))
        maTab[i, "logA"] <- x
        maTab[i, "logFC"] <- y
        maTab[i, "p.val"] <- wilcox.test(ROI, Other)$p.val
        latVar[[as.character(maTab[i, "m/z"])]] <- as.vector(mzSlice)
      }
      
      #
      row.names(maTab) <- attributes(slices())$center
      maTab$p.val <- ifelse(maTab$p.val <= 1e-16, 1e-16, maTab$p.val)
      maTab$FDR <- p.adjust(maTab$p.val, method = "fdr")
      maLatVar <- list("maTab" = maTab, "latVar" = latVar)
    })
    
    # Render image registration
    output$manRegImage <- renderPlot({
      cols =  heat_hcl(100, h = c(0, 250), c. = c(30, 150),
                       l = c(100, 25), power = c(1/5, 3),
                       gamma = NULL, fixup = TRUE, alpha = 0.75)
      # layout(matrix(c(1, 2, 3, 3), 2, 2, byrow = TRUE), heights = c(1, 1))
      par(mfrow = c(1, 2))
      
      # Sample picture and  m/z slice
      nmask <- images()[["nmask"]]
      imgReg <- images()[["imgReg"]]
      mzSlice <- images()[["mzSlice"]]
      
      # Mask
      image((nmask), main = "ROI")
      
      # Sample picture to MSI 
      image(Image((imgReg)))
      image(Image((mzSlice)), col = cols, add = TRUE)
    })
    
    # ROI univariate analisis MAplot
    output$maplot <- renderPlot({
      maTab <- maLatVar()$maTab
      topN <- 10
      topUp <- row.names(maTab[order(maTab$logA * maTab$logFC, decreasing = TRUE),])[1:topN]
      topDown <- row.names(maTab[order(maTab$logA * -maTab$logFC, decreasing = TRUE),])[1:topN]
      plot(maTab$logA, maTab$logFC, xlab = "log10(A)", ylab = "log2(FC)", 
           col = ifelse(row.names(maTab) %in% c(topUp, topDown), "red", "black"),
           pch = ifelse(row.names(maTab) %in% c(topUp, topDown), 19, 20),
           lwd = 3,
           main = "MAplot, ROI vs. Other")
      grid()
      abline(h = 0, lwd = 2)
      points(maTab[1, "logA"], maTab[1, "logFC"], pch = 3, lwd = 5, col = "blue3")
      text(maTab[c(topUp, topDown), "logA"], maTab[c(topUp, topDown), "logFC"], 
           labels = as.character(round(as.numeric(c(topUp, topDown)), 2)), pos = 3)
      
    })
    
    # ROI univariate analisis table
    output$mzTab <- renderDataTable({
      maLatVar()$maTab
      # head(tab)
    })
    
    
    # ROI multi-variate analisis, PCA
    output$latvarPCAPlot <- renderPlot({
      
      #
      tab <- do.call(rbind, maLatVar()$latVar)
      nmask <- images()$nmask
      
      #
      tab[is.na(tab)] <- 0
      colnames(tab) <- ifelse(as.vector(nmask == 1), "ROI", "Other")
      
      #
      p <- prcomp(t(tab), scale. = TRUE)
      nPC <- input$nPCpca
      par(mfrow = c(nPC*1, 2))
      cols0Pca <- colorRampPalette((brewer.pal(11, "PRGn")))(100)
      for (i in 1:nPC) {
        # if (sum(p$x[names(p$x[, i]) == "ROI", i]) > sum(p$x[names(p$x[, i]) == "Other", i])) cols <- rev(cols0Pca)
        barplot(p$rotation[, i], las = 3, border = FALSE, 
                col = ifelse(p$rotation[, i] > 0, cols0Pca[length(cols0Pca)-5+1], cols0Pca[5]),
                names.arg = round(as.numeric(row.names(p$rotation)), 2),
                main = paste0("m/z contribution\nPrin. Comp. 1, ", 
                              round(p$sdev[i]**2/sum(p$sdev**2)*100, 2), "% Variance"))
        grid()
        scoreMat <- (matrix(p$x[, i], nrow = nrow(nmask), ncol = ncol(nmask)))
        image(Image((scoreMat)), col = cols0Pca)
        
      }
    })
    
    # ROI multi-variate analisis, sPLS-DA
    output$latvarsPLSDAPlot <- renderPlot({
      
      #
      tab <- do.call(rbind, maLatVar()$latVar)
      nmask <- images()$nmask
      
      #  
      tab[is.na(tab)] <- 0
      colnames(tab) <- ifelse(as.vector(nmask == 1), "ROI", "Other")
      
      #
      X <- t(tab)
      X <- t(apply(X, 1, function(x) (x/max(x))*2 -1 ))
      row.names(X) <- NULL #paste0(row.names(X), seq(nrow(X)))
      nPC <- input$nPCsplsda
      pda <- splsda(X = as.matrix(X), Y = factor(ifelse(colnames(tab) == "ROI", 1, -1)),
                    ncomp = nPC, scale = TRUE) #, keepX = round(input$topNval*0.75))
      pdaX <- predict(pda, X)
      
      #    
      par(mfrow = c(nPC, 2))
      colsSplsDa <- colorRampPalette(rev(brewer.pal(11, "RdBu")))(100)
      for (i in 1:nPC) {
        barplot(pdaX$B.hat[, "1", i], las = 3, border = FALSE,
                col = ifelse(pdaX$B.hat[, "1", i] > 0, colsSplsDa[length(colsSplsDa)-5+1], colsSplsDa[5]),
                names.arg = round(as.numeric(row.names(pdaX$B.hat)), 2))
        # main = paste0("m/z contribution\nPrin. Comp. 1, ", 
        #               round(p$sdev[i]**2/sum(p$sdev**2)*100, 2), "% Variance"))
        grid()
        varMat <- (matrix(pdaX$variates[, i], nrow = nrow(nmask), ncol = ncol(nmask)))
        varMat <- (matrix(pdaX$predict[, "1", 1], nrow = nrow(nmask), ncol = ncol(nmask)))
        # if (sum(p$x[names(p$x[, i]) == "ROI", i]) > sum(p$x[names(p$x[, i]) == "Other", i])) cols <- rev(cols0)
        image(Image((varMat)), col = colsSplsDa)
      }
    })
    
    
    # MultiSlice 
    output$multipleSpectra <- renderPlot({
      slices <- msiSlices(imagespectra, center = c(input$mzcenter1, input$mzcenter2, input$mzcenter3, input$mzcenter4),
                          tolerance = input$mztoleranceMulti)
      plotMsiSlice(slices, combine = TRUE,
                   colRamp = list(colorRamp(brewer.pal(8, "Blues")),
                                  colorRamp(brewer.pal(8, "Greens")),
                                  colorRamp(brewer.pal(8, "Reds")),
                                  colorRamp(brewer.pal(8, "Greys"))))
      
      
    })
    
    
  })
  
  
})



# reactive({
# imagespectra <- importImzMl(input$imzMLfile, centroided=TRUE)
# imagespectra <- topN(imagespectra, n=100)
# mzdensity <- density(unlist(lapply(imagespectra, mass)), bw=0.01)
# })
# output$ <- renderText("Fertsch")
# output$msiPlot <- renderPlot({plot(mzdensity, xlab="m/z", ylab="density", main="m/z density distribution")})
#})
