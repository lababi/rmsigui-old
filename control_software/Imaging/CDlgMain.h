// **************************************************************************************
// DEFINICION DE LA CLASE CMainDlg
// **************************************************************************************

class CDlgMain : protected CMsg {

protected:

	 CDlgMain() {}
	~CDlgMain() {}

private:

	LRESULT InitDialog( HWND, UINT, WPARAM, LPARAM );
	LRESULT Show( HWND, UINT, WPARAM, LPARAM );
	LRESULT Cancel( HWND, UINT, WPARAM, LPARAM );
	LRESULT SysCommand( HWND, UINT, WPARAM, LPARAM );
	LRESULT Validate( HWND, UINT, WPARAM, LPARAM );
	LRESULT SetPosition( HWND, UINT, WPARAM, LPARAM );
	LRESULT LaserFound( HWND, UINT, WPARAM, LPARAM );
	LRESULT LaserRecall( HWND, UINT, WPARAM, HWND );
	LRESULT LaserSet( HWND, UINT, WPARAM, LPARAM );
	LRESULT LaserEnable( HWND, UINT, WPARAM, LPARAM );

protected:

	static EVENT       SystemEvents[];
	static EVENT       CommandEvents[];
	static EVENT_INFO  EventInfo[];

	UINT   Intensity;

	CWndProcThunk Thunk;
};
