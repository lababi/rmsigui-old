#include <windows.h>
#include <shlobj.h>
#include <tchar.h>

#include "..\\_include\\TWindow.h"
#include "..\\_include\\CSerial.h"
#include "..\\_include\\CCoord.h"
#include "..\\_include\\CDrawTable.h"
#include "..\\_include\\CSnoop.h"
#include "CWControl.h"

typedef TWindow<CWControl> CControl;

#define  FOCUS_MARK_SIZE      8
#define  ARROW_IMAGE_ID     200
#define  ARROW_GRAY_ID      201
#define  ARROW_IMAGE_WIDTH  108
#define  MASK_BYTE_COUNT    ARROW_IMAGE_WIDTH * ( ARROW_IMAGE_WIDTH + 31 ) * 4 / 32

#define  IDT_TIMER           20
#define  BUTTON_PRESS         0
#define  BUTTON_RELEASE      -1
#define  MOVE_PLANE           0
#define  MOVE_SPACE           1

#define THERMO_START   0x1FB7
#define THERMO_STATUS  0x1FCB
#define THERMO_SUFFIX  0x224D


enum { AS_REAL = 0, AS_INTEGER, AS_TIME };
enum { KEY_NEXTWIN = 1, KEY_SETCOORD, KEY_HOME, KEY_TABLE, KEY_TESTRGN, KEY_IMAGING };
enum { STORE_X = 0,      STORE_Y,     STORE_Z,
       STORE_MINX,       STORE_MINY,  STORE_MAXX,   STORE_MAXY, 
	   STORE_STEP,       STORE_WIDTH, STORE_HEIGHT, STORE_AREA,  STORE_PERIOD,
	   STORE_RESOLUTION, STORE_COLS,  STORE_ROWS,   STORE_COUNT, STORE_TIME,
       STORE_IMAGING };
enum { DO_SERIAL_VALIDATE = WM_USER, DO_SET_AMPLITUDE, 
       DO_TESTREGION, DO_IMAGING, DO_ALERT };
enum { IDS_SERIAL = 40000, IDS_SERIALOPEN, IDS_SERIALID, 
       IDS_SERIALVALIDATE, IDS_SERIALWRITE, 
       IDS_THERMO, IDS_THERMOFIND, IDS_THERMOLOST, IDS_SYNCHRO, IDS_SYNCHROFILE };



// **************************************************************************************
// AREA DE DATOS
// **************************************************************************************
CWControl::EVENT  CWControl::SystemEvents[] = {
	WM_TIMER,           reinterpret_cast<CMsg::HANDLER>( &CWControl::NextScan ),
	DO_IMAGING,         reinterpret_cast<CMsg::HANDLER>( &CWControl::SetupScans ),
	DO_TESTREGION,      reinterpret_cast<CMsg::HANDLER>( &CWControl::TestRegion ),
	WM_KEYDOWN,         reinterpret_cast<CMsg::HANDLER>( &CWControl::KeyDown ),
	WM_KEYUP,           reinterpret_cast<CMsg::HANDLER>( &CWControl::KeyUp ),
	WM_LBUTTONDOWN,     reinterpret_cast<CMsg::HANDLER>( &CWControl::LButtonDown ),
	WM_LBUTTONUP,       reinterpret_cast<CMsg::HANDLER>( &CWControl::LButtonUp ),
	WM_SETFOCUS,        reinterpret_cast<CMsg::HANDLER>( &CWControl::Focus ),
	WM_KILLFOCUS,       reinterpret_cast<CMsg::HANDLER>( &CWControl::Focus ),
	WM_GETDLGCODE,      reinterpret_cast<CMsg::HANDLER>( &CWControl::GetDlgCode ),
	WM_PAINT,           reinterpret_cast<CMsg::HANDLER>( &CWControl::Paint ),
	WM_SETTEXT,         reinterpret_cast<CMsg::HANDLER>( &CWControl::SetText ),
	WM_ENABLE,          reinterpret_cast<CMsg::HANDLER>( &CWControl::Enable ),
	WM_NCDESTROY,       reinterpret_cast<CMsg::HANDLER>( &CWControl::NCDestroy ),
	WM_CREATE,          reinterpret_cast<CMsg::HANDLER>( &CWControl::Create ),
	DO_SERIAL_VALIDATE, reinterpret_cast<CMsg::HANDLER>( &CWControl::InitMakerMex ),
	DO_ALERT,           reinterpret_cast<CMsg::HANDLER>( &CWControl::Alert ),
	DO_SET_AMPLITUDE,   reinterpret_cast<CMsg::HANDLER>( &CWControl::SetAmplitude ),
};

CWControl::EVENT_INFO  CWControl::EventInfo[] = {
	SystemEvents,  sizeof(SystemEvents)/sizeof(EVENT),
};

BYTE  CWControl::Mask[ MASK_BYTE_COUNT ];
UINT  CWControl::Column[5];
UINT  CWControl::KeyToIndex[] = { 2, 2, 0, 0, 0, 1, 0, 1 };


CDrawTable::TString CWControl::Label[] = {
	_T("Position"), 8,									//  0 Titulos tabla Posicion
	_T("X"), 1, _T("Y"), 1, _T("Z"), 1,					//  1 Leyendas
	_T("Limits"), 6, _T("Min"), 3, _T("Max"), 3,		//  4 Titulos tabla Region
	_T("Horizontal"), 10, _T("Vertical"), 8,			//  7 Leyendas
	_T("Parameter"), 9, _T("[mm]"), 4, _T("Pixel"), 5,	//  9 Titulos tabla Stats
	_T("Resolution"), 10, _T("Width"), 5,				// 12 Leyendas
	_T("Height"), 6, _T("Area"), 4, _T("Time"), 4
};


// **************************************************************************************
// REGISTRO DE VENTANA
// **************************************************************************************
UINT RegisterChild( HINSTANCE instance ) {

	return RegisterControl( 
				instance, 
				_T("LABI_Makermex"), 
				reinterpret_cast<WNDPROC>( &CControl::ControlProc ),
				0
			);
}


// **************************************************************************************
// PROCESO DE EVENTOS DEL SISTEMA
// **************************************************************************************
LRESULT CWControl::Create( HWND hwnd, UINT msg, WPARAM wparam, LPCREATESTRUCT create ) {

	KeyCode      = 0;
	IsFocusOwner = 0;
	IsWorking    = 0;
	IsWaiting    = 0;
	DeltaZ       = 0;
	hFile        = 0;
	HwndParent   = GetParent( hwnd );
	hWnd         = hwnd;
	Update       = &CWControl::UpdateSpectrum;

	// Comunicacion con el Dispositivo de movimiento
	Robot.EnumArduinoPorts( MAKER_MEX );
	if( !Robot.IsConnected() ) {
		::ShowAlert( HwndParent, IDS_SERIAL, IDS_SERIALOPEN );
		return -1;
	}

	// Deshabilita el control de movimiento
	EnableWindow( hwnd, FALSE );

	// Comunicacion con el l�ser
	Laser.EnumArduinoPorts( ARDUINO_UNO );

	// Calculo de las dimensiones de las ventanas
	HDC   hdc  = GetDC( hwnd );
	HFONT font = CDrawTable::SetFont( hdc );
	MeasureColumns( hdc, font );
	ReleaseDC( hwnd, hdc );

	// Inicia los objetos CDrawTable
	InitTables();

	// Ajusta el tama�o de la ventana
	SetControlSize( hwnd, 
		ShowStats.Column[2],
		ShowStats.Top + ( ShowStats.nRow + 1 ) * ShowStats.Height );

	// Carga la imagen de los botones
	LoadArrow( create->hInstance );
	return Arrow ? 0 : -1;

}


LRESULT CWControl::InitMakerMex( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Valida el mensaje inicial
	char string[1024];
	Robot.Message    = "start";
	Robot.MessageLen = 5;

	if( !Robot.Read( string, 1024, &CSerial::FindMessage ) ) {
		ShowAlert( HwndParent, IDS_SERIAL, IDS_SERIALID );
		PostMessage( HwndParent, WM_CLOSE, 0, 0 );
		return FALSE;
	}

	// Lectura del mensaje con la configuracion del robot
	UINT lines = Robot.LineCounting( string );
	
	while( lines < 22 ) {
		Robot.Read( string, 1024, &CSerial::FindNewLine );
		lines += Robot.LineCounting( string );
	} 

	// Mensaje para comparar exito de la operacion
	Robot.Message    = "ok";
	Robot.MessageLen = 2;
	EnableWindow( hwnd, TRUE );

	// El microcontrolador Arduino regresa "LABI Laser"
	// cuando esta programado para controlar el l�ser microSystems
	if( Laser.IsConnected() ) {
		Laser.Message    = "LABI Laser\r";
		Laser.MessageLen = 11;

		// Notifica a la ventana padre cuando el laser esta activo
		if( Laser.Read( string, 256, &CSerial::FindMessage ) )
			SendMessage( 
				GetParent( hwnd ), 
				WM_COMMAND,
				(BN_CLICKED << 16) | GetDlgCtrlID( hwnd ), 0 );
	}

	return 0;
}


LRESULT CWControl::NCDestroy( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Borra los bitmaps
	DeleteObject( ArrowGray );
	DeleteObject( Arrow );

	// Borra las fuentes
	CDrawTable::FreeResources();

	// Elimina el temporizador en caso de error
	if( IsWorking ) {
		KillTimer( hwnd, IDT_TIMER );
		IsWorking = 0;
	}

	delete this;
	return 0;
}


LRESULT CWControl::Enable( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	HBITMAP choose[] = { Arrow, ArrowGray };

	// Habilita el bitmap
	Display     = choose[ wparam == 0 ];

	// Actualiza el despliegue
	InvalidateRect( hwnd, 0, TRUE );
	UpdateWindow( hwnd );
	return 0;
}


// **************************************************************************************
// RUTINAS DE DESPLIEGUE
// **************************************************************************************
LRESULT CWControl::Paint( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	PAINTSTRUCT ps;
	HDC         hdc = BeginPaint ( hwnd, &ps );

	// Dibuja el bitmap
	HDC hdcSource = CreateCompatibleDC( hdc );
	SelectObject( hdcSource, Display );
	BitBlt( hdc, 0, 0, ARROW_IMAGE_WIDTH, ARROW_IMAGE_WIDTH, hdcSource, 0, 0, SRCCOPY );

	// Dibuja la marca del foco de entrada
	UINT color     = ( IsFocusOwner & COLOR_BTNSHADOW ) | ( ~IsFocusOwner & COLOR_MENU );
	HGDIOBJ oldPen = SelectObject( hdc, CreatePen( PS_NULL, 1, 0 ) );
	SelectObject( hdc, GetSysColorBrush( color ) );
	Ellipse( hdc, *Column + 4, ARROW_IMAGE_WIDTH - FOCUS_MARK_SIZE - 13,
		          *Column + 4 + FOCUS_MARK_SIZE, ARROW_IMAGE_WIDTH - 13 );
	DeleteObject( SelectObject( hdc, oldPen ) );

	// Despliega las coordenadas de la posicion actual 
	ShowCoord.DrawTitle( hdc );
	ShowCoord.DrawValues( hdc );
	ShowRegion.DrawHeaders( hdc );
	ShowRegion.DrawValues( hdc );
	ShowStats.DrawHeaders( hdc );
	ShowStats.DrawValues( hdc );

	// Libera los recursos
	DeleteDC( hdcSource );
	EndPaint( hwnd, &ps );
	return 0;
}


void CWControl::DrawState( HDC hdcTarget, UINT key, UINT state ) {

	static UINT limits[][4] = {
		 73,  1,  1, -1,		// VK_PRIOR
		  1, 73, -1,  1,		// VK_NEXT
		 73, 73,  1,  1,		// VK_END
		 37, 37,  0, -1,		// VK_HOME
		  1, 37, -1,  0,		// VK_LEFT
		 37,  1,  0, -1,		// VK_UP
		 73, 37,  1,  0,		// VK_RIGHT
		 37, 73,  0,  1,		// VK_DOWN
	};

	// Calculo del desplazamiento del boton
	key        -= VK_PRIOR;
	UINT *coord = &limits[key][0];
	UINT  width = 35 + ( state & 2 );

	// Dibuja el boton
	HDC hdcSource = CreateCompatibleDC( hdcTarget );
	SelectObject( hdcSource, Arrow );

	BitBlt( hdcTarget, 
		    coord[ 0 ] + ( coord[ 2 ] | state ), 
		    coord[ 1 ] + ( coord[ 3 ] | state ), width, width, hdcSource, 
			coord[ 0 ] + state, 
			coord[ 1 ] + state, SRCCOPY );

	// Libera los rescursos
	DeleteDC( hdcSource );
}


void CWControl::UpdateRegion( HDC hdc, UINT axis ) {

	// Valores para el ajuste de los indices
	axis      -= VK_LEFT;
	axis       = 3 & ( 4 - axis );			// { 0, 3, 2, 1 };
	UINT  min  = 1 & axis;					// { 0, 1, 0, 1 };
	UINT  max  = 2 + min;					// { 2, 3, 2, 3 };

	// Carga la coordenada en la tabla
	UINT  *region  = Store + STORE_MINX;
	region[ axis ] = Store[ axis & 1 ];

	// Actualiza la tabla Region
	UINT   xchng   = region[ min ] > region[ max ];

	switch( xchng ) {

	case 1:		// Ordena los valores
		axis          = max;
		xchng         = region[ max ];
		region[ max ] = region[ min ];
		region[ min ] = xchng;

		Num2String( STORE_MINX + min, AS_REAL );
		ShowRegion.DrawCell( hdc, min );

	default:	// Convierte a Texto
		Num2String( STORE_MINX + axis, AS_REAL );
		ShowRegion.DrawCell( hdc, axis );
	}
}


void CWControl::UpdateStats( HDC hdc ) {

	UINT   area, mask;
	UINT  *stats  = Store + STORE_STEP;
	UINT  *region = Store + STORE_MINX;

	// Actualiza la tabla Stats
	switch( stats[0] ) {

	default:  // Calcula los parametros en pixels
		stats[6] = ( region[2] - region[0] ) / stats[0] + 1;
		stats[7] = ( region[3] - region[1] ) / stats[0] + 1;
		stats[8] = stats[6] * stats[7];

		Num2String( STORE_COLS, AS_INTEGER );
		Num2String( STORE_ROWS, AS_INTEGER );
		Num2String( STORE_COUNT, AS_INTEGER );

		// Actualiza el tiempo aproximado del experimento
		Store[ STORE_TIME ]   = Store[ STORE_PERIOD ] * Store[ STORE_COUNT ] / 100;
		Num2String( STORE_TIME, AS_TIME );

	case 0:  // Calcula los parametros en milimetros
		stats[1] = region[2] - region[0];
		stats[2] = region[3] - region[1];

		Num2String( STORE_WIDTH, AS_REAL );
		Num2String( STORE_HEIGHT, AS_REAL );

		// Imprime decimales para areas menores a 400 mm^2
		area     = stats[1] * stats[2];
		mask     = area > 4000000;
		stats[3] = area;

		UINT divisor[] = { 100, 10000, 50, 5000 };
		stats[3]       = area / divisor[ mask ] + 
						 ( ( area % divisor[ mask ] ) >= ( divisor[ mask + 2 ] ) );

		Num2String( STORE_AREA, AS_REAL + mask );
	}
	
	ShowStats.DrawValues( hdc );
}


// **************************************************************************************
// MANEJO DEL TECLADO
// **************************************************************************************
LRESULT CWControl::Focus( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	IsFocusOwner = 0 - ( msg == WM_SETFOCUS );
	InvalidateRect( hwnd, NULL, FALSE );
	return 0;
}


LRESULT CWControl::GetDlgCode( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	return DLGC_WANTALLKEYS;
}


LRESULT CWControl::KeyDown( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	HDC  hdc;

	// Obtiene el estado de las teclas especiales
	UINT isAlt   = - ( ( GetKeyState( VK_MENU ) & 0x10000000 ) != 0 );
	UINT isShft  = - ( ( GetKeyState( VK_SHIFT ) & 0x8000 )    != 0 );
	UINT isCtrl  = - ( ( GetKeyState( VK_CONTROL ) & 0x8000 )  != 0 );
	UINT onlyAlt =   ( isAlt  & ~( isCtrl | isShft ) );	

	// Agrupa las teclas por categoria
	UINT isHome  = -( wparam == VK_HOME );
	UINT isEnd   = -( wparam == VK_END );
	UINT isSpace = ~( isHome | isEnd ) & ( ( wparam - VK_PRIOR ) <= VK_DOWN ? -1 : 0 );
	UINT isPlane =  ( wparam - VK_LEFT ) <= VK_DOWN ? -1 : 0;

	// Determina la accion a seguir
	UINT action  = ( wparam == VK_TAB ) |
				   ( KEY_SETCOORD & isSpace & ~isAlt  & ~( isCtrl & isShft ) ) |
				   ( KEY_HOME     & isHome  & onlyAlt ) |
				   ( KEY_TABLE    & isPlane & onlyAlt ) |
				   ( KEY_TESTRGN  & isEnd   & isShft  & ~( isCtrl | isAlt ) ) |
				   ( KEY_IMAGING  & isEnd   & onlyAlt );

	switch( -( KeyCode == 0 ) & action ) {

	case KEY_HOME:
		Position->Horizontal = 0;
		Position->Vertical   = 0;
		Position->Elevation  = 0;

	case KEY_SETCOORD:			// Actualiza el registro de posicion
		SetKeyIncrement( wparam, isAlt, isShft, isCtrl );

	default:					// Dibuja el boton presionado
		KeyCode = LOWORD( wparam ) | ( action << 16 );
		hdc     = GetDC( hwnd );
		DrawState( hdc, wparam, BUTTON_PRESS );
		ReleaseDC( hwnd, hdc );

	case 0:						// Tecla ordinaria
		return 0;

	case KEY_NEXTWIN:			// VK_TAB
		HWND next;
		next = GetNextDlgTabItem( HwndParent, hwnd, isShft );
		SetFocus( next );
		return 0;
	}
}


LRESULT CWControl::KeyUp( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Termina si no hay proceso pendientes
	if( !KeyCode )
		return FALSE;

	UINT index = KeyToIndex[ wparam - VK_PRIOR ];
	HDC  hdc   = GetDC( hwnd );

	// Actualiza el estado de los botones
	DrawState( hdc, wparam, BUTTON_RELEASE );

	// Realiza la accion
	switch( HIWORD( KeyCode ) ) {

	case KEY_SETCOORD:		// Movimiento por eje
		SendCommand( 0, MOVE_SPACE );
		ShowCoord.DrawCell( hdc, index );
		break;

	case KEY_HOME:			// Reinicia el sistema coordenado
		ShowCoord.DrawValues( hdc );
		SendCommand( "G28\r", 4 );
		break;

	case KEY_TABLE:			// Actualiza la tabla Region
		UpdateRegion( hdc, wparam );
		UpdateStats( hdc );
		break;

	case KEY_TESTRGN:		// Prueba de la region de muestreo
		PostMessage( hwnd, DO_TESTREGION, 0, 0 );
		break;

	case KEY_IMAGING:		// Inicia el experimento
		PostMessage( hwnd, DO_IMAGING, 0, 0 );
		break;
	}

	ReleaseDC( hwnd, hdc );
	return KeyCode = 0;
}


void CWControl::SetKeyIncrement( UINT wparam, UINT isAlt, UINT isShft, UINT isCtrl ) {

	UINT               index   = KeyToIndex[ wparam - VK_PRIOR ];
	UINT              *current = Store + index;

	// Obtiene el incremento: (ctrl) 1 = 0.1 mm;  10 = 1 mm;  (alt) 100 = 10 mm
	UINT  bounds[2];
	bounds[0]  = ( isCtrl & 10 ) | ( ~isCtrl & ~isShft & 100 ) | ( isShft & 1000 );

	// Actualiza las coordenadas
	switch( wparam ) {

	default:
		return;

	case VK_NEXT:			// Movimiento hacia abajo
	case VK_UP:				// Movimiento hacia enfrente
	case VK_RIGHT:			// Movimiento hacia derecho
		bounds[1]   = *current;
		current[0] -=  bounds[ *current < bounds[0] ];
		break;

	case VK_PRIOR:          // Movimiento hacia arriba
	case VK_DOWN:			// Movimiento hacia atras
	case VK_LEFT:           // Movimiento hacia la izquierda
		current[0] += bounds[0];
		break;

	case VK_HOME:           // Posicion inicial
		Num2String( STORE_Y, AS_REAL );
		Num2String( STORE_Z, AS_REAL );
	}

	Num2String( current - Store, AS_REAL );
}


// **************************************************************************************
// MANEJO DEL MOUSE
// **************************************************************************************
LRESULT CWControl::LButtonDown( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	if( !IsFocusOwner )
		SetFocus( hwnd );

	// Las coordenadas siempre estan entre 0 y ARROW_IMAGE_WIDTH - 1
	UINT x = LOWORD( lparam );
	UINT y = HIWORD( lparam );
	
	x = static_cast<int>( -( x < ARROW_IMAGE_WIDTH ) ) & x;
	y = static_cast<int>( -( y < ARROW_IMAGE_WIDTH ) ) & ( ARROW_IMAGE_WIDTH - 1 - y );

	// Verifica si se presiona sobre un boton

	BYTE  *region      = Mask + ( y * 16 + x / 8 );
	BYTE   bitPosition = 0x80 >> ( x & 0x7 );
	if( !( *region & (bitPosition) ) )
		return 0;

	SetCapture( hwnd );

	// Determina el boton presionado

	x = ( x > 36 ) + ( x > 72 );
	y = ( y > 36 ) + ( y > 72 );

	static UINT key[] = { 
		VK_NEXT, VK_DOWN,  VK_END,
		VK_LEFT, VK_HOME,  VK_RIGHT,
		0,       VK_UP,    VK_PRIOR,
	};

	KeyDown( hwnd, WM_KEYDOWN, key[y*3 + x], lparam );
	return 0;
}


LRESULT CWControl::LButtonUp( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	ReleaseCapture();
	if( KeyCode ) {
		KeyUp( hwnd, WM_KEYUP, LOWORD( KeyCode ), 0 );
	}

	return 0;
}



// **************************************************************************************
// CREACION DE LOS OBJETOS GDI
// **************************************************************************************
void CWControl::LoadArrow( HINSTANCE instance ) {

	// Crea los dispositivos de memoria
	HDC hdc       = CreateIC( _T( "DISPLAY" ), 0, 0, 0 );
	HDC hdcSource = CreateCompatibleDC( hdc );
	HDC hdcTarget = CreateCompatibleDC( hdc );
	DeleteDC( hdc );

	// Modifica el fondo de las imagenes
	Arrow     = LoadBitmap( instance, MAKEINTRESOURCE( ARROW_IMAGE_ID ) );
	ArrowGray = LoadBitmap( instance, MAKEINTRESOURCE( ARROW_GRAY_ID ) );
	ChangeBackground( instance, hdcSource, hdcTarget );

	// Bitmap en colores como imagen principal
	Display   = ArrowGray;

	// Limpieza
	DeleteDC( hdcTarget );
	DeleteDC( hdcSource );	
}


void CWControl::ChangeBackground( HINSTANCE instance, HDC hdcSource, HDC hdcTarget ) {

	// Carga la mascara
	HBITMAP mask  = LoadBitmap( instance, MAKEINTRESOURCE( ARROW_GRAY_ID + 1 ) );

	// Modifica el fondo de las imagenes
	SelectObject( hdcTarget, GetSysColorBrush( COLOR_MENUBAR ) );
	SelectObject( hdcSource, mask );
	SelectObject( hdcTarget, ArrowGray );
	BitBlt( hdcTarget, 0, 0, ARROW_IMAGE_WIDTH, ARROW_IMAGE_WIDTH,
		    hdcSource, 0, 0, 0x00B8074A );

	SelectObject( hdcTarget, Arrow );
	BitBlt( hdcTarget, 0, 0, ARROW_IMAGE_WIDTH, ARROW_IMAGE_WIDTH,
		    hdcSource, 0, 0, 0x00B8074A );

	// Obtiene los bits de la mascara
	struct {
		BITMAPINFOHEADER  header;
		RGBQUAD           palette[2];
	} BmpInfo = { sizeof( BITMAPINFOHEADER ), ARROW_IMAGE_WIDTH, ARROW_IMAGE_WIDTH, 
		          1, 1, BI_RGB };

	GetDIBits( hdcTarget, mask, 0, ARROW_IMAGE_WIDTH, Mask, 
			   reinterpret_cast<BITMAPINFO *>( &BmpInfo ), DIB_RGB_COLORS );

	// Libera los recursos - Las brochas del sistema no deben destruirse
	DeleteObject( mask );
}


void CWControl::MeasureColumns( HDC hdc, HFONT font ) {

	HGDIOBJ lastGDI = SelectObject( hdc, font );

	// Tabla posicion actual
	TCHAR  numeric[] = _T("XXXX000.00");
	SIZE   dimension;
	GetTextExtentPoint32( hdc, numeric, 10, &dimension );
	Column[0] = ARROW_IMAGE_WIDTH + dimension.cx / 8;
	Column[1] = Column[0] + dimension.cx;

	// Tabla con informacion de la imagen
	GetTextExtentPoint32( hdc, _T("HorizontalX"), 11, &dimension );
	Column[2] = 0;
	Column[4] = dimension.cx;

	GetTextExtentPoint32( hdc, numeric + 1, 9, &dimension );
	Column[4] += 2 * dimension.cx;

	// Selecciona la longitud mayor
	UINT index = Column[4] > Column[1] ? 4 : 1;
	Column[1] = Column[ index ];
	Column[3] = Column[ index ] - dimension.cx;
	Column[4] = Column[ index ];

	// Ancho de las columnas numericas
	GetTextExtentPoint32( hdc, numeric + 3, 7, &dimension );
	CDrawTable::ColWidth = dimension.cx;
	SelectObject( hdc, lastGDI );
}


void CWControl::InitTables() {

	// Inicia el apuntador
	Position = reinterpret_cast<CCoord *>( Store );

	// Configuracion de las tablas
	UINT  properties[] = {
	//	Row, Col, Top, iCol, iLab, iRen, nLgn 
		  3,   2,   0,    0,    0,    0,    1,		// ShowCoord
		  2,   3,   0,    2,    4,    6,    7,		// ShowRegion
		  5,   3,   0,    2,    9,   12,   12,		// ShowStats
	};

	CDrawTable *table[] = { &ShowCoord, &ShowRegion, &ShowStats };
	CDrawTable::TString *targetVal = Values;
	CDrawTable::TString *sourceVal;
	TCHAR               *render = &Render[0][0];

	// Inicia los objetos

	UINT  *object  = properties;
	UINT  *target;
	UINT  *current = Store;
	UINT   i;
	UINT   j = 0;

	do {		
		// Inicializa los objetos
		target = &table[j]->nRow;

		*target ++ = *object ++;
		*target ++ = *object ++;
		*target ++ = *object ++;
		*target ++ = reinterpret_cast<UINT>( Column + *object ++ );
		*target ++ = reinterpret_cast<UINT>( Label  + *object ++ );
		*target ++ = reinterpret_cast<UINT>( Values + *object ++ );

		// Copia las leyendas de los renglones
		i         = *( object - 6 );
		sourceVal = Label + *( object );

		do {
			targetVal->String     = sourceVal->String;
			targetVal ++ ->Length = sourceVal ++ ->Length;
		} while( -- i );

		// Copia las direcciones de los valores y limpia los campos
		i  = object[ -6 ] * ( object[ -5 ] - 1 );

		do {
			lstrcpyn( render, _T("0.00"), 5 );
			*current ++           = 0;
			targetVal->String     = render;
			targetVal ++ ->Length = 4;
			render               += 8;
		} while( -- i );

		object ++;

	} while ( ++ j < 3 );

	ShowRegion.Top = ARROW_IMAGE_WIDTH + CDrawTable::Height / 2;
	ShowStats.Top  = ShowRegion.Top    + ShowRegion.Height / 2 +
					 ShowRegion.Height * ( ShowRegion.nRow + 1 );

	//// Longitud 1 desde STORE_AREA hasta STORE_COUNT
	//i          = STORE_COUNT - STORE_RESOLUTION + 1;
	//targetVal -= i;

	//do {
	//	targetVal++ ->Length = 1;
	//} while( -- i );

	// Condiciones de prueba para algoritmo de muestreo
	//Store[ STORE_MINX ]   = 1100;	Num2String( STORE_MINX, AS_REAL );
	//Store[ STORE_MINY ]   = 2100;	Num2String( STORE_MINY, AS_REAL );
	//Store[ STORE_MAXX ]   = 1400;	Num2String( STORE_MAXX, AS_REAL );
	//Store[ STORE_MAXY ]   = 2400;	Num2String( STORE_MAXY, AS_REAL );
	//Store[ STORE_STEP ]   = 100;	Num2String( STORE_STEP, AS_REAL );
	//Store[ STORE_WIDTH ]  = 300;	Num2String( STORE_WIDTH, AS_REAL );
	//Store[ STORE_HEIGHT ] = 300;	Num2String( STORE_HEIGHT, AS_REAL );

	//Store[ STORE_AREA ]   = 900;	Num2String( STORE_AREA, AS_REAL );
	//Store[ STORE_RESOLUTION ] = 1;	Num2String( STORE_RESOLUTION, AS_INTEGER );
	//Store[ STORE_COLS ]   = 4;		Num2String( STORE_COLS, AS_INTEGER );
	//Store[ STORE_ROWS ]   = 4;		Num2String( STORE_ROWS, AS_INTEGER );
	//Store[ STORE_COUNT ]  = 16;		Num2String( STORE_COUNT, AS_INTEGER );
	//Store[ STORE_PERIOD ] = 50;		Num2String( STORE_PERIOD, AS_REAL );
}


// **************************************************************************************
// MENSAJES PARA COMUNICACION
// **************************************************************************************
LRESULT CWControl::SetText( HWND hwnd, UINT msg, WPARAM wparam, TCHAR *buffer ) {

	if( IsWorking )						// Si hay experimento en curso rechaza la orden
		return FALSE;

	// Modifica el modo de operacion
	switch( *buffer ) {

	case _T('F'):
		CWControl::SystemEvents[0].Dispatch = 
			reinterpret_cast<CMsg::HANDLER>( &CWControl::NextSpectrum );
		CWControl::SystemEvents[1].Dispatch = 
			reinterpret_cast<CMsg::HANDLER>( &CWControl::SetupSpectrum );
		Update = &CWControl::UpdateElevation;
		DeltaZ = 10;
		return TRUE;

	case _T('L'):
		CWControl::SystemEvents[0].Dispatch = 
			reinterpret_cast<CMsg::HANDLER>( &CWControl::NextSpectrum );
		CWControl::SystemEvents[1].Dispatch = 
			reinterpret_cast<CMsg::HANDLER>( &CWControl::SetupSpectrum );
		Update = &CWControl::UpdateLaser;
		DeltaZ = 0;
		return TRUE;

	case _T('I'):
		CWControl::SystemEvents[0].Dispatch = 
			reinterpret_cast<CMsg::HANDLER>( &CWControl::NextSpectrum );
		CWControl::SystemEvents[1].Dispatch = 
			reinterpret_cast<CMsg::HANDLER>( &CWControl::SetupSpectrum );
		Update = &CWControl::UpdateSpectrum;
		DeltaZ = 0;
		return TRUE;

	case _T('S'):
		CWControl::SystemEvents[0].Dispatch = 
			reinterpret_cast<CMsg::HANDLER>( &CWControl::NextScan );
		CWControl::SystemEvents[1].Dispatch = 
			reinterpret_cast<CMsg::HANDLER>( &CWControl::SetupScans );
		DeltaZ = 0;
		return TRUE;
	}

	// Comandos para movimiento y almacenamiento de resolucion
	HDC hdc = GetDC( hwnd );

	if( *buffer == _T('M') ) {			// Actualiza la posicion
		
		Position->Scan( buffer + 2 );
		SetKeyIncrement( VK_HOME, 0, 0, 0 );
		ShowCoord.DrawValues( hdc );
		SendCommand( 0, MOVE_PLANE );
	}

	else if( *buffer == _T('R') ) {		// Actualiza la resolucion y el retardo

		CCoord  Move;
		Move.Scan( buffer + 2 );
		Store[ STORE_STEP ]       = Move.Horizontal;
		Store[ STORE_RESOLUTION ] = 100 / Move.Horizontal;
		Store[ STORE_PERIOD ]     = Move.Vertical;

		Num2String( STORE_PERIOD, AS_REAL );
		Num2String( STORE_STEP, AS_REAL );
		Num2String( STORE_RESOLUTION, AS_INTEGER );
		UpdateStats( hdc );
	}

	ReleaseDC( hwnd, hdc );
	return TRUE;
}


UINT CWControl::SendCommand( char *string, UINT length ) {

	static char command[64] = { 'G', '0', '1', ' ' };
	UINT   errorMSG = IDS_SERIALVALIDATE;

	// Genera el comando por defecto
	if( !string ) {
		string          = command;
		length          = Position->Format2CNC( command + 4, length ) + 4;
		command[ length ++ ] = '\r';
		command[ length ]    = 0;
	}

	// Envia el comando
	char status[32];
	if( Robot.Send( string, length ) ) {
		//OutputDebugStringA( command );
		if( Robot.Read( status, 64, &CSerial::FindMessage ) ) {
			//OutputDebugStringA( status );
			return TRUE;
		}

		errorMSG ++;
	}

	// Manejo de errores
	CloseExperiment();
	PostMessage( hWnd, DO_ALERT, IDS_SERIAL, errorMSG );
	return FALSE;
}


UINT CWControl::Num2String( UINT field, UINT option ) {

	static TCHAR  *timeFormat[] = { _T("%u'%u"), _T("%u:%u") };

	UINT index = ( ( field > STORE_Z ? -1 : 0 ) & 2 ) + 3 +
				 ( ( field > STORE_MAXY  ? -1 : 0 ) & 5 ) + field;

	CDrawTable::TString  *label = Values + index;
	UINT seconds, hours;

	// Parte decimal
	switch( option ) {

	case AS_REAL:
		return label->Length = CCoord::Print( label->String, Store[ field ] );

	case AS_INTEGER:
		return label->Length = wsprintf( label->String, _T("%u"), Store[ field ] );

	case AS_TIME:
		hours   = Store[ field ] > 3600 ? -1 : 0;
		seconds = Store[ field ] / ( ( hours & 59 ) + 1 );
		return label->Length = wsprintf( label->String, timeFormat[ hours & 1 ], 
								seconds / 60, seconds % 60 );
	}

	return 0;
}


// **************************************************************************************
// CONTROL DEL LASER
// **************************************************************************************
LRESULT CWControl::SetAmplitude( HWND hwnd, UINT msg, WPARAM strLen, TCHAR *string ) {

	// Actualiza la intensidad del l�ser y limpia el buffer
	union {
		char   cmd[8];
		TCHAR  buffer[8];
	};

	cmd[0] = LOBYTE( string[0] );
	cmd[1] = LOBYTE( string[1] );
	cmd[2] = LOBYTE( string[2] );

	Laser.Send( cmd, strLen );
	Laser.Read( cmd, sizeof( cmd ), &CSerial::FindNewLine );

	//char txt[16];
	//wsprintfA( txt, ":%s\n", string );
	//OutputDebugStringA( txt );

	return 0;
}


// **************************************************************************************
// MENSAJES PARA EL CONTROL DE EXPERIMENTOS
// **************************************************************************************
LRESULT CWControl::TestRegion( HWND hwnd, UINT msg, WPARAM, LPARAM ) {

	// Coordenadas de la frontera y punto inicial
	UINT rectangle[] = {
		Store[ STORE_MINX ], Store[ STORE_MINY ],
		Store[ STORE_MINX ], Store[ STORE_MAXY ],
		Store[ STORE_MAXX ], Store[ STORE_MAXY ],
		Store[ STORE_MAXX ], Store[ STORE_MINY ],
		Store[ STORE_MINX ], Store[ STORE_MINY ],
		Store[ STORE_X ],    Store[ STORE_Y ]
	};

	// Mueve el dispositivo en las fronteras de la region
	UINT i        = 0;
	UINT *current = rectangle;
	do {
		Store[ STORE_X ] = *current ++ ;
		Store[ STORE_Y ] = *current ++ ;
		SendCommand( 0, MOVE_PLANE );
	} while( ++ i < 6 );

	return 0;
}


void CWControl::ActiveThermo() {

	// La ventana activa debe ser "Acquire Data"
	if( GetForegroundWindow() != Thermo.Window  )
		SetForegroundWindow( Thermo.Window );

	// Activa / Desactiva el espectrometro
	SendDlgItemMessage( Thermo.Window, THERMO_START, BM_CLICK, 0, 0 );
}


LRESULT CWControl::Alert( HWND hwnd, UINT msg, WPARAM caption, LPARAM message ) {

	ShowAlert( HwndParent, caption, message );
	PostMessage( hwnd, WM_QUIT, 0, 0 );
	return TRUE;
}


UINT CWControl::InitializeExperiment() {

	// Deshabilita la ventana
	EnableWindow( hWnd, FALSE );
	IsWorking = TRUE;
	LaserPower = 50;

	// Lo minimo que se puede hacer es una linea
	if( !Store[ STORE_ROWS ] & !Store[ STORE_COLS ] )
		return 0;

	// Establece comunicacion con ThermoFisher
	if( !Thermo.Find( _T("Acquire Data"), 12 ) ) {
		ShowAlert( hWnd, IDS_THERMO, IDS_THERMOFIND );
		return 0;
	}

	// Establece las condiciones iniciales
	Step[0]  = static_cast<UINT>( 0 - Store[ STORE_STEP ] );
	Step[1]  = Store[ STORE_STEP ];
	Col      = 0;
	Row      = 1;
	FileID   = 0;

	// Posicion inicial, x - step, y
	Position = reinterpret_cast<CCoord*>( Coord );
	Position->Horizontal = Store[ STORE_MINX ] - Store[ STORE_STEP ];
	Position->Vertical   = Store[ STORE_MINY ];
	Position->Elevation  = Store[ STORE_Z ] + DeltaZ;

	return SendCommand( 0, MOVE_SPACE );
}


void CWControl::CloseExperiment() {

	// Cierra el archivo
	if( hFile != 0 ) {
		CloseHandle( hFile );
		hFile     = 0;
	}

	// Elimina el temporizador
	KillTimer( hWnd, IDT_TIMER );
	IsWorking = 0;
	IsWaiting = 0;
	Position  = reinterpret_cast<CCoord *>( Store );
}


// **************************************************************************************
// ADQUISICION DE PIXELES POR SCAN
// **************************************************************************************
LRESULT CWControl::SetupScans( HWND hwnd, UINT msg, WPARAM, LPARAM ) {

	// Inicia el experimento y crea el archivo de sincronizacion
	if( !InitializeExperiment() || !CreateSynchroFile() )
		return 0;

	// Inicia el temporizador
	SetTimer( hwnd, IDT_TIMER, Store[ STORE_PERIOD ] * 10, 0 );

	// Inicia la adquisicion
	ActiveThermo();
	return 0;
}


LRESULT CWControl::NextScan( HWND hwnd, UINT msg, WPARAM, LPARAM ) {

	enum { IMG_THERMO_LOST = 0, IMG_NEXT, IMG_END };

	// Determina la accion a seguir
	UINT   imsWin = 0 - ( IsWindow( Thermo.Window ) != 0 );
	UINT   done   = ( Col == Store[ STORE_COLS ] ) & ( Row == Store[ STORE_ROWS ] );
	UINT   action = imsWin & ( 1 + done );

	// Inicia la adquisicion
	switch( action ) {

	case IMG_THERMO_LOST:
		PostMessage( hwnd, DO_ALERT, IDS_THERMO, IDS_THERMOLOST );
		break;

	case IMG_END:
		Position  = reinterpret_cast<CCoord *>( Store );
		ScanSetup();					// Determina coordenadas y sufijo del pixel
		SendCommand( 0, MOVE_SPACE );	// Cambia la posicion
		ActiveThermo();					// Termina la adquiscion
		break;

	case IMG_NEXT:
		ScanSetup();					// Mueve el dispositivo
		SendCommand( 0, MOVE_PLANE );
		return 0;
	}

	CloseExperiment();
	EnableWindow( hwnd, TRUE );
	return 0;
}


void CWControl::ScanSetup() {

	// Calcula el indice del pixel y lo almacena en FileID
	PixelIndex();

	// Obtiene el tiempo transcurrido
	CWControl::CTime elapsed;
	GetSystemTimeAsFileTime( &elapsed.Time );
	elapsed.Utc = ( elapsed.Utc - StartTime ) / 10000;

	// Formato de la cadena
	char   synchro[32];
	UINT   length;
	length = wsprintfA( synchro, "%.6u,%u\n", FileID, elapsed.Time.dwLowDateTime );

	// Salva el archivo
	DWORD cbWrite;
	WriteFile( hFile, &synchro, length, &cbWrite, 0 );
}


UINT CWControl::CreateSynchroFile() {

	// Obtiene la ruta del escritorio
	TCHAR fileName[ MAX_PATH ];
	SHGetSpecialFolderPath( hWnd, fileName, CSIDL_DESKTOP, 0 );
	lstrcat( fileName, _T("\\Synchro.txt") );

	// Creacion del archivo
	hFile = CreateFile( fileName, GENERIC_WRITE, 0, 0, CREATE_ALWAYS,
				FILE_ATTRIBUTE_NORMAL, 0 );

	if( !hFile ) {
		ShowAlert( hWnd, IDS_SYNCHRO, IDS_SYNCHROFILE );
		return 0;
	}

	// Obtiene la hora inicial
	CWControl::CTime *start;
	start = reinterpret_cast<CWControl::CTime *>( &StartTime );
	GetSystemTimeAsFileTime( &start->Time );

	// Preserva el UTC por separado en las primeras dos lineas de la tabla
	char startTime[64];
	DWORD cbWritten;
	UINT  length;
	length = wsprintfA( startTime, "%.6u,%u\n%.6u,%u\n", 
				0, start->Time.dwLowDateTime, 0, start->Time.dwHighDateTime );
	WriteFile( hFile, startTime, length, &cbWritten, 0 );

	return TRUE;
}



// **************************************************************************************
// ADQUISICION DE PIXELES POR ESPECTRO
// **************************************************************************************
LRESULT CWControl::SetupSpectrum( HWND hwnd, UINT msg, WPARAM, LPARAM ) {

	if( InitializeExperiment() ) {

		// Inicia el temporizador
		SetTimer( hwnd, IDT_TIMER, 200, 0 );

		// Deshabilita la ventana
		EnableWindow( hwnd, FALSE );
		IsWorking = TRUE;
		IsWaiting = FALSE;
	}

	return 0;
}


LRESULT CWControl::NextSpectrum( HWND hwnd, UINT msg, WPARAM, LPARAM ) {

	enum { IMG_THERMO_LOST = 0, IMG_NEXT, IMG_WAIT, IMG_SUCCESS };

	// Determina la accion a seguir
	UINT   imsWin = 0 - ( IsWindow( Thermo.Window ) != 0 );
	UINT   done   = ( Col == Store[ STORE_COLS ] ) & ( Row == Store[ STORE_ROWS ] );
	UINT   action = imsWin & ( 1 + IsWaiting + done * 2 );

	// Inicia la adquisicion
	switch( action ) {

	case IMG_THERMO_LOST:
		PostMessage( hwnd, DO_ALERT, IDS_THERMO, IDS_THERMOLOST );
		break;

	case IMG_SUCCESS + 1:
	case IMG_SUCCESS:
		Position  = reinterpret_cast<CCoord *>( Store );
		SendCommand( 0, MOVE_SPACE );
		break;

	case IMG_WAIT:
		if( IsThermoBusy() )
			return 0;

	case IMG_NEXT:

		// Actualiza las coordenadas y el sufijo del archivo
		(*this.*Update)();

		// Inicia el espectrometro
		ActiveThermo();
		IsWaiting = 1;
		return 0;

	}

	CloseExperiment();
	EnableWindow( hwnd, TRUE );
	return 0;
}


UINT CWControl::IsThermoBusy() {

	static  UINT  busy;

	TCHAR label[32];
	UINT  length, isIdle;

	// Obtiene el mensaje de la ventana
	length  = SendDlgItemMessage( Thermo.Window, THERMO_STATUS, WM_GETTEXT, 32,
				reinterpret_cast<LPARAM>( label ) );

	// Secuencia de adquisicion Idle -> Pending -> Acquare -> Idle
	isIdle = CompareString
		     ( LOCALE_SYSTEM_DEFAULT, 0, label, length, _T("Idle"), 4 ) == 2;

	// Actualiza el estado
	IsWaiting = 1 ^ ( isIdle & busy );
	busy      = 1 ^ isIdle;

	return IsWaiting;
}


UINT CWControl::UpdateSpectrum() {

	// Calcula el sufijo del pixel
	PixelIndex();

	// Actualiza el sufijo del archivo
	TCHAR  suffix[12];
	SendDlgItemMessage( Thermo.Window, THERMO_SUFFIX, WM_SETTEXT, 
		wsprintf( suffix, _T("%.5u"), FileID ),
		reinterpret_cast<LPARAM>( suffix ) );

	// Mueve el dispositivo
	return SendCommand( 0, MOVE_PLANE );
}


void CWControl::PixelIndex() {

	// Calcula las nuevas coordenas
	UINT mask   = ( Col != Store[ STORE_COLS ] ) ? -1 : 0;
	Col         = ( Col & mask ) + 1;
	Row        += ~mask &  1;

	Coord[0]   +=  mask & Step[ Row & 1 ];
	Coord[1]   += ~mask & Step[ 1 ];

	// Almacena el sufijo del archivo correspondiente
	FileID  += ( ~mask & Store[ STORE_COLS ] ) + 
		       (  mask & ( ( Row & 1 ) ? 1 : -1 ) );
}


// **************************************************************************************
// PROCEDIMIENTO DE ENFOQUE
// **************************************************************************************
UINT CWControl::UpdateElevation() {

	// Calcula el sufijo del pixel
	Coord[2] -= DeltaZ;
	PixelIndex();

	// Actualiza el sufijo del archivo
	TCHAR  suffix[12];
	SendDlgItemMessage( Thermo.Window, THERMO_SUFFIX, WM_SETTEXT, 
		wsprintf( suffix, _T("%u"), Coord[2] ),
		reinterpret_cast<LPARAM>( suffix ) );

	// Mueve el dispositivo
	return SendCommand( 0, MOVE_SPACE );
}


// **************************************************************************************
// EFECTO DE LA POTENCIA DEL LASER
// **************************************************************************************
UINT CWControl::UpdateLaser() {

	// Calcula el sufijo del pixel
	PixelIndex();

	// Condiciones para fin de ciclo al alcanzar la potencia maxima del laser
	if( LaserPower <= 15 ) {
		Col = Store[ STORE_COLS ];
		Row = Store[ STORE_ROWS ];
	}

	// Actualiza el sufijo del archivo
	TCHAR  suffix[12];
	UINT   strLen = wsprintf( suffix, _T("%u"), LaserPower -- );
	SendDlgItemMessage( Thermo.Window, THERMO_SUFFIX, WM_SETTEXT, 
		strLen,	reinterpret_cast<LPARAM>( suffix ) );

	// Convierte caracteres e incrementa la potencia del laser
	SetAmplitude( hWnd, DO_SET_AMPLITUDE, strLen, suffix );

	// Mueve el dispositivo
	return SendCommand( 0, MOVE_PLANE );
}

/* NOTAS DE DISE�O

1) Un numero de 16 bits puede representar 65535 estados
2) En terminos de coordenadas seria 655.35
3) Esto equivale a 65.5 centimetros de desplazamiento de la mesa
4) El desplazamiento maximo del robot es 20.cm
5) El numero maximo que se puede covertir con multiplicacion por 10 es 4,000

*/
