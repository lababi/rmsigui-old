#include <windows.h>
#include <tchar.h>

#include "..\\_include\\TWindow.h"
#include "..\\_include\\CCoord.h"

#include "CDlgMain.h"
#include "CDlgAbout.h"
#include "CDlgHelp.h"
#include "resource.h"

#define DO_SERIAL_VALIDATE    WM_USER
#define DO_SET_AMPLITUDE      WM_USER + 1

extern UINT RegisterChild( HINSTANCE );



// **************************************************************************************
// AREA DE DATOS
// **************************************************************************************
CDlgMain::EVENT  CDlgMain::SystemEvents[] = {
	WM_SYSCOMMAND,  reinterpret_cast<CMsg::HANDLER>( &CDlgMain::SysCommand ),
	WM_INITDIALOG,  reinterpret_cast<CMsg::HANDLER>( &CDlgMain::InitDialog ),
	WM_NCDESTROY,   reinterpret_cast<CMsg::HANDLER>( &TWindow<CDlgMain>::NCDestroy ),
	WM_SHOWWINDOW,  reinterpret_cast<CMsg::HANDLER>( &TWindow<CDlgMain>::Show )
};

CDlgMain::EVENT  CDlgMain::CommandEvents[] = {
	(EN_CHANGE << 16) | IDC_XPOS, reinterpret_cast<CMsg::HANDLER>( &Validate ),
	(EN_CHANGE << 16) | IDC_YPOS, reinterpret_cast<CMsg::HANDLER>( &Validate ),
	(EN_CHANGE << 16) | IDC_STEP, reinterpret_cast<CMsg::HANDLER>( &Validate ),
	(EN_CHANGE << 16) | IDC_DELAY, reinterpret_cast<CMsg::HANDLER>( &Validate ),
	(BN_CLICKED << 16) | IDC_MOVE, reinterpret_cast<CMsg::HANDLER>( &SetPosition ),
	(BN_CLICKED << 16) | IDC_SETUP, reinterpret_cast<CMsg::HANDLER>( &SetPosition ),
	(BN_CLICKED << 16) | IDC_CUSTOM1, reinterpret_cast<CMsg::HANDLER>( &LaserFound ),
	(BN_CLICKED << 16) | IDC_LASERSET, reinterpret_cast<CMsg::HANDLER>( &LaserSet ),
	(BN_CLICKED << 16) | IDC_LASEROFF, reinterpret_cast<CMsg::HANDLER>( &LaserEnable ),
	(BN_CLICKED << 16) | IDCANCEL, reinterpret_cast<CMsg::HANDLER>( &Cancel ),
	(EN_KILLFOCUS << 16) | IDC_AMPLITUDE, reinterpret_cast<CMsg::HANDLER>( &LaserRecall ),
};

CDlgMain::EVENT_INFO  CDlgMain::EventInfo[] = {
	SystemEvents,  sizeof( SystemEvents ) / sizeof( EVENT ),
	CommandEvents, sizeof( CommandEvents ) / sizeof( EVENT )
};




// **************************************************************************************
// PUNTO DE ENTRADA AL PROGRAMA
// **************************************************************************************
extern "C" int WINAPI WinMain
( HINSTANCE instance, HINSTANCE hPrevInst, PSTR szCmdLine, int iCmdShow ) {

	RegisterChild( instance );
	TWindow<CDlgMain>::DlgBox( instance,  IDD_DIALOG1, GetDesktopWindow(), SDlgProc );
	return 0;
}



// **************************************************************************************
// PROCESO DE EVENTOS DEL SISTEMA
// **************************************************************************************
LRESULT CDlgMain::InitDialog( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Ajusta el tama�o de la ventana
	RECT rc;
	GetControlRect( GetDlgItem( hwnd, IDC_CUSTOM1 ), &rc );
	SetWindowRect( hwnd, rc.top + rc.right, rc.top / 2 + rc.bottom );

	// Centra la ventana y modifica el icono
	CenterWindow( hwnd );

	HINSTANCE instance = (HINSTANCE) GetWindowLong( hwnd, GWL_HINSTANCE );
	SetIconWindow( instance, hwnd, IDI_ICON1 );

	// Agrega los elementos del experimento al menu
	HMENU hMenu = GetSystemMenu( hwnd, FALSE );
    AppendMenu( hMenu, MF_SEPARATOR, 0, NULL );
	AppendMenu( hMenu, MF_STRING, IDM_ABOUT, _T("About LABI Imaging ...") );
	AppendMenu( hMenu, MF_STRING, IDM_HELP, _T("Labi Imaging Help ...") );
    AppendMenu( hMenu, MF_SEPARATOR, 0, NULL );
    AppendMenu( hMenu, MF_STRING, IDM_IMAGE_SINGLE, _T("Image single spectrum") );
	AppendMenu( hMenu, MF_STRING, IDM_IMAGE_MULTIPLE, _T("Image multiple spectrum") );
	CheckMenuRadioItem( hMenu, IDM_IMAGE_SINGLE, IDM_IMAGE_MULTIPLE, 
			IDM_IMAGE_SINGLE, MF_BYCOMMAND );

	return TRUE;
}


LRESULT CDlgMain::Show( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Valida la comunicacion del puerto serial dentro del control de movimiento
	HWND hChild = GetDlgItem( hwnd, IDC_CUSTOM1 );
	PostMessage( hChild, DO_SERIAL_VALIDATE, 0, 0 );
	return TRUE;
}


LRESULT CDlgMain::SysCommand( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	HINSTANCE instance;
	TCHAR     cmd[] = _T("SIFL");

	// Opciones del Menu
	UINT isValidMsg = LOWORD( wparam ) - IDM_ABOUT;

	switch( isValidMsg ) {

	case 0:					// IDM_ABOUT
		instance = (HINSTANCE) GetWindowLong( hwnd, GWL_HINSTANCE );
		TWindow<CDlgAbout>::DlgBox( instance, IDD_DIALOG2, hwnd, SDlgProc );
		return 0;

	case 1:                // IDM_HELP
		instance = (HINSTANCE) GetWindowLong( hwnd, GWL_HINSTANCE );
		TWindow<CDlgHelp>::DlgBox( instance, IDD_DIALOG3, hwnd, SDlgProc );
		return 0;

	case 2:                // IDM_IMAGE_SINGLE
	case 3:                // IDM_IMAGE_MULTIPLE
	case 4:                // IDM_FOCUSING
	case 5:                // IDM_GRADIENT
		if( SendDlgItemMessage
		( hwnd, IDC_CUSTOM1, WM_SETTEXT, 1, 
		reinterpret_cast<LPARAM>( cmd + isValidMsg - 2 ) ) ) {
		
			HMENU hMenu= GetSystemMenu( hwnd, FALSE );
			CheckMenuRadioItem( hMenu, IDM_IMAGE_SINGLE, IDM_GRADIENT, 
				LOWORD( wparam ), MF_BYCOMMAND );
		}
	}

	return 0;	
}


LRESULT CDlgMain::Cancel( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	EndDialog( hwnd, TRUE );
	return TRUE;
}



// **************************************************************************************
// WM_COMMAND
// **************************************************************************************
LRESULT CDlgMain::Validate( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	/* static struct isValid {
		unsigned int xPos       : 1;
		unsigned int yPos       : 1;
		unsigned int Resolution : 1;
		unsigned int Delay      : 1;
	} */

	static  UINT isValid;

	// Calcula la posicion de la bandera
	UINT  identifier = LOWORD( wparam );
	UINT  index      = identifier - IDC_XPOS;
	UINT  flag       = 1 << index;
	UINT  mask       = 0xF ^ flag;

	// Obtiene el texto de la ventana
	TCHAR  buffer[16];
	UINT   digit = SendDlgItemMessage( hwnd, identifier, 
						WM_GETTEXT, 16, reinterpret_cast<UINT>( buffer ) );

	// Digito menos significativo es multiplo de 5?
	digit -= digit != 0;
	digit  = ( buffer[ digit ] - _T('0') ) % 5;
	digit  = 0 - ( digit == 0 );

	// Valida la entrada
	isValid = ( isValid & mask ) | 
		      ( ( 0 - CCoord::Validate( buffer ) ) & flag & digit );
	
	// Obtiene el estado del boton correspondiente
	HWND child  = GetDlgItem( hwnd, IDC_MOVE + ( index > 1 ) );
	UINT enable = IsWindowEnabled( child );

	// Habilita la ventana
	static UINT xchng[] = { 3, 3, 0xC, 0xC };
	mask = xchng[ index ];
	flag = ( ( isValid & mask ) == mask );

	if( flag ^ enable ) {
		EnableWindow( child, enable ^ 1 );
	}

	return TRUE;
}


LRESULT CDlgMain::SetPosition( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Decodifica la accion a seguir
	TCHAR   buffer[24];
	TCHAR  *current = buffer;
	TCHAR   axis    = _T('X');
	TCHAR   cmd[]   = _T("MR");
	UINT    index   = ( LOWORD( wparam ) != IDC_MOVE );

	// Formato del comando
	*current ++ = cmd[ index ];
	*current ++ = _T(' ');

	*current ++ = axis ++;
	 current   += SendDlgItemMessage( hwnd, IDC_XPOS + ( index << 1 ), 
					WM_GETTEXT, 24, reinterpret_cast<LPARAM>( current ) );

	*current ++ = axis;
	 current   += SendDlgItemMessage( hwnd, IDC_YPOS + ( index << 1 ), 
					WM_GETTEXT, 24, reinterpret_cast<LPARAM>( current ) );

	// Actualiza el control de movimiento
	SendDlgItemMessage
	( hwnd, IDC_CUSTOM1, WM_SETTEXT, 0, reinterpret_cast<LPARAM>( buffer ) );
	return 0;
}



// **************************************************************************************
// FUNCIONES PARA EL MANEJO DEL LASER
// **************************************************************************************
LRESULT CDlgMain::LaserFound( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Agrega los elementos del menu que involucran al laser
	HMENU hMenu = GetSystemMenu( hwnd, FALSE );
	AppendMenu( hMenu, MF_STRING, IDM_FOCUSING, _T("Laser Focusing") );
	AppendMenu( hMenu, MF_STRING, IDM_GRADIENT, _T("Laser Gradient") );

	// Habilita los controles del laser
	EnableWindow( GetDlgItem( hwnd, IDC_AMPLITUDE ), TRUE );
	EnableWindow( GetDlgItem( hwnd, IDC_LASERSET ), TRUE );
	EnableWindow( GetDlgItem( hwnd, IDC_LASEROFF ), TRUE );
	EnableWindow( GetDlgItem( hwnd, IDC_LABELINT ), TRUE );
	SetDlgItemInt( hwnd, IDC_AMPLITUDE, 0, 0 );
	Intensity = 0;

	return 0;
}


LRESULT CDlgMain::LaserRecall( HWND hwnd, UINT msg, WPARAM wparam, HWND hCtrl ) {

	TCHAR  string[8];
	TCHAR *current   = string;
	LPARAM param     = reinterpret_cast< LPARAM >( string );
	UINT   amplitude = 0;
	UINT   strlen    = SendMessage( hCtrl, WM_GETTEXT, 8, param );

	switch( strlen ) {

	case 0:			// Recupera el ultimo valor cuando el campo esta vacio
		SetDlgItemInt( hwnd, IDC_AMPLITUDE, Intensity, FALSE );
		break;

	case 2:
		amplitude = ( *current ++ - _T('0') ) * 10;

	case 1:
		amplitude += ( *current - _T('0') );
		if( amplitude < 50 )
			break;

	default:		// El valor m�ximo es 49 unidades
		string[0] = '4';
		string[1] = '9';
		string[2] = 0;
		strlen    = 2;
		SendMessage( hCtrl, WM_SETTEXT, 0, param );
	}

	return TRUE;
}


LRESULT CDlgMain::LaserSet( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	SetCursor( LoadCursor( 0, IDC_WAIT ) );

	// Obtiene el valor de la intensidad
	TCHAR  string[8];
	LPARAM param  = reinterpret_cast<LPARAM>( string );
	UINT   strlen = SendDlgItemMessage( hwnd, IDC_AMPLITUDE, WM_GETTEXT, 8, param );

	// Cambia la intensidad de salida del laser y limpia el buffer
	SendDlgItemMessageA( hwnd, IDC_CUSTOM1, DO_SET_AMPLITUDE, strlen, param );
	Intensity = GetDlgItemInt( hwnd, IDC_AMPLITUDE, 0, 0 );

	// Restaura el cursor
	SetCursor( LoadCursor( 0, IDC_ARROW ) ); 
	return TRUE;
}


LRESULT CDlgMain::LaserEnable( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	SetCursor( LoadCursor( 0, IDC_WAIT ) ); 

	// Habilitar ?
	UINT index  = IsWindowEnabled( GetDlgItem( hwnd, IDC_AMPLITUDE ) ) == 0;
	UINT enable = 0 - index;

	// Modifica la intensidad del Laser
	SetDlgItemInt( hwnd, IDC_AMPLITUDE, Intensity & enable, 0 );

	UINT intensity = Intensity;
	LaserSet( hwnd, WM_COMMAND, 0, 0 );
	Intensity = intensity;

	// Habilita los botones
	EnableWindow( GetDlgItem( hwnd, IDC_AMPLITUDE ), enable );
	EnableWindow( GetDlgItem( hwnd, IDC_LASERSET ), enable );

	// Cambia el valor del despliegue de intensidad
	TCHAR   *caption[2] = { _T("On"), _T("Off") };
	LPARAM  *param      = reinterpret_cast<LPARAM*>( caption );
	SendDlgItemMessage( hwnd, IDC_LASEROFF, WM_SETTEXT, 0, param[ index ] );

	// Restaura el cursor
	SetCursor( LoadCursor( 0, IDC_ARROW ) ); 
	return TRUE;
}