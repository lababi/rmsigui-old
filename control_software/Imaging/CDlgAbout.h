// **************************************************************************************
// DEFINICION DE LA CLASE CAboutDlg
// **************************************************************************************

class CDlgAbout : protected CMsg {

protected:

	 CDlgAbout() {}
	~CDlgAbout() {}

private:

	LRESULT InitDialog( HWND, UINT, WPARAM, LPARAM );
	LRESULT OwnerDraw( HWND, UINT, WPARAM, DRAWITEMSTRUCT* );
	LRESULT EraseBkgnd( HWND, UINT, HDC, LPARAM );
	LRESULT OkCancel( HWND, UINT, WPARAM, LPARAM );

protected:

	static EVENT       SystemEvents[];
	static EVENT       CommandEvents[];
	static EVENT_INFO  EventInfo[];

    HINSTANCE  hInstance;
	UINT       LineHeight;

	CWndProcThunk Thunk;
};
