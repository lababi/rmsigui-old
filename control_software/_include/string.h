// **************************************************************************************
// DEFINICIONES
// **************************************************************************************

UINT __fastcall CopyStringW(LPWSTR target, LPWSTR source);
UINT __fastcall CloneSegmentW(LPWSTR target, LPWSTR source);
UINT __fastcall ExtractSegmentW(LPWSTR path);
UINT __fastcall StringLengthW(LPWSTR source);

#ifdef UNICODE
#define CloneSegment    CloneSegmentW
#define ExtractSegment  ExtractSegmentW
#define CopyString      CopyStringW
#define StringLength    StringLengthW
#endif

