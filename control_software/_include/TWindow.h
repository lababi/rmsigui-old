#include "Thunk.h"


// **************************************************************************************
// PROCEDIMIENTOS DE VENTANA
// **************************************************************************************

LRESULT __stdcall SWndProc( HWND, UINT, WPARAM, LPARAM );
LRESULT __stdcall ClassWndProc( HWND, UINT, WPARAM, LPARAM );

LRESULT __stdcall SupWndProc( HWND, UINT, WPARAM, LPARAM );
LRESULT __stdcall SubWndProc( HWND, UINT, WPARAM, LPARAM );

LRESULT  CALLBACK SDlgProc( HWND, UINT, WPARAM, LPARAM );
LRESULT  CALLBACK SNDlgProc( HWND, UINT, WPARAM, LPARAM );



// **************************************************************************************
// RUTINAS GENERALES
// **************************************************************************************

HWND CreateFrame( HINSTANCE, WNDPROC, UINT, HMENU );
UINT RegisterControl( HINSTANCE, TCHAR *, WNDPROC, UINT );
UINT ShowAlert( HWND, UINT, UINT );

void CenterWindow( HWND );
void SetIconWindow( HINSTANCE, HWND, int );
void SetControlSize( HWND hwnd, UINT, UINT );
void GetControlRect( HWND hwnd, RECT * );
void SetWindowRect( HWND, UINT, UINT );


// **************************************************************************************
// DEFINICION DE LA CLASE CMsg
// **************************************************************************************

class CMsg {

public:

	typedef LPARAM (CMsg::*HANDLER)( HWND, UINT, WPARAM, LPARAM );

	struct EVENT {
		UINT     Message;
		HANDLER  Dispatch;
	};

	struct EVENT_INFO {
		EVENT   *Message;
		UINT     Elements;
	};

	struct EVENT_INFOCLASS {
		EVENT   *Message;
		UINT     Elements;
		UINT    *Offset;
	};
};



// **************************************************************************************
// DEFINICION DE LA CLASE TWindow
// **************************************************************************************

template<class CWnd> 
class TWindow : public CWnd {

protected:

	 TWindow<CWnd>() {};
	~TWindow<CWnd>() {};

public:

	LRESULT  NCDestroy( HWND, UINT, WPARAM, LPARAM );

	static HWND    Main( HINSTANCE, WNDPROC, UINT style, HMENU );
	static BOOL    DlgBox( HINSTANCE, UINT id, HWND parent, WNDPROC, LPARAM param = 0 );

	static ATOM    SuperWndReg( HINSTANCE, LPTSTR controlName,  LPTSTR className );
	static LRESULT SuperWndProc( HWND, UINT, WPARAM, LPARAM );

	static ATOM    SubClassWnd( HWND hwnd );
	static LRESULT SubClassWndProc( HWND, UINT, WPARAM, LPARAM );

	static LRESULT ControlProc( HWND, UINT, WPARAM, LPARAM );
};



// **************************************************************************************
// DESTRUCTOR
// **************************************************************************************

template<typename CWnd> LRESULT TWindow<CWnd>::NCDestroy
( HWND, UINT, WPARAM, LPARAM ) {

	delete this;
	return 0;
}



// **************************************************************************************
// VENTANA PRINCIPAL
// **************************************************************************************

template<typename CWnd> HWND TWindow<CWnd>::Main
( HINSTANCE instance, WNDPROC wndProc, UINT style, HMENU menu ) {

	// Crea el objeto

	static TWindow<CWnd> window;

	// Inicia el thunk

	union {
		WNDPROC         WndProc;
		CWndProcThunk  *Thunk;
	};

	WndProc = wndProc;
	window.Thunk.Init( wndProc, &window, &CWnd::EventInfo );

	// Crea la ventana con un THUNK como procedimiento

	Thunk = &window.Thunk;
	return CreateFrame( instance, WndProc, style, menu );
}




// **************************************************************************************
// CUADRO DE DIALOGO
// **************************************************************************************

template<typename CWnd> BOOL TWindow<CWnd>::DlgBox
( HINSTANCE instance, UINT id, HWND hwndParent, WNDPROC wndProc, LPARAM param ) {

	// Crea el objeto

	TWindow<CWnd> *window = new TWindow<CWnd>;

	// Inicia el thunk

	union {
		DLGPROC         dlgProc;
		CWndProcThunk  *thunk;
	};

	window->Thunk.Init( wndProc, window, &CWnd::EventInfo );

	// Crea la ventana con un THUNK como procedimiento

	thunk = &window->Thunk;
	return DialogBoxParam( instance, MAKEINTRESOURCE(id), hwndParent, dlgProc, param );
}



// **************************************************************************************
// SUPER CLASIFICACION DE VENTANA
// **************************************************************************************

template<typename CWnd> ATOM TWindow<CWnd>::SuperWndReg
(HINSTANCE instance, LPTSTR controlName,  LPTSTR className) {

	// Obtiene las propiedades del control de ventana

	WNDCLASSEX wc;
	GetClassInfoEx(NULL, controlName, &wc);
	TWindow<CWnd>::g_BaseProc = wc.lpfnWndProc;

	// Superclasifica el control

	wc.cbSize        = sizeof(WNDCLASSEX);
	wc.lpfnWndProc   = reinterpret_cast<WNDPROC>( &TWindow<CWnd>::SuperWndProc );
	wc.hInstance     = instance;
	wc.lpszClassName = className;

	return RegisterClassEx(&wc);
}



template<typename CWnd> LRESULT TWindow<CWnd>::SuperWndProc
( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Crea el objeto

	TWindow<CWnd> *window = new TWindow<CWnd>;

	// Inicia el thunk

	union {
		WNDPROC         wndProc;
		CWndProcThunk  *thunk;
		LONG            setWindow;
	};

	window->Thunk.Init( g_WndProc, window, &CWnd::EventInfo, CWnd::g_BaseProc );

	// Modifica el procedimiento de ventana

	thunk = &window->Thunk;
	SetWindowLong( hwnd, GWL_WNDPROC, setWindow );
	return CallWindowProc( g_BaseProc, hwnd, msg, wparam, lparam );
}



// **************************************************************************************
// CONTROL
// **************************************************************************************

template<typename CWnd> LRESULT TWindow<CWnd>::ControlProc
( HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam ) {

	// Crea el objeto

	TWindow<CWnd> *window = new TWindow<CWnd>;

	// Inicia el thunk

	union {
		WNDPROC         wndProc;
		CWnd::HANDLER   handler;
		CWndProcThunk  *thunk;
		LONG            setWindow;
	};

	wndProc = &SWndProc;
	window->Thunk.Init( wndProc, window, &CWnd::EventInfo );

	// Modifica el procedimiento de ventana

	thunk = &window->Thunk;
	SetWindowLong( hwnd, GWL_WNDPROC, setWindow );
	return (window->*handler)( hwnd, msg, wparam, lparam );
}


// **************************************************************************************
// SUB CLASIFICACION DE VENTANA
// **************************************************************************************

/*
template<typename CWnd> LRESULT TWindow<CWnd>::SubClassWnd( HWND hwnd ) {

	// Crea el objeto

	TWindow<CWnd> *window = new TWindow<CWnd>;

	// Inicia el thunk

	union {
		WNDPROC         wndProc;
		CWnd::HANDLER   handler;
		CWndProcThunk  *thunk;
		LONG            setWindow;
	};

	wndProc = &SubWndProc;
	window->OldProc = GetWindowLong( hwnd, GWL_WNDPROC );
	window->Thunk.Init( wndProc, window, &CWnd::EventInfo, window->OldProc );

	// Modifica el procedimiento de ventana

	thunk = &window->Thunk;
	return SetWindowLong( hwnd, GWL_WNDPROC, setWindow ) != 0;
}


*/