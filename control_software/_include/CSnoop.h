class CSnoop {

	static UINT CALLBACK ReciveWindowHandle( HWND hwnd, CSnoop* );

public:

	CSnoop() {}
   ~CSnoop() {}

	UINT Find( TCHAR *string, UINT length );

private:
	static  TCHAR  *AppName;
	static  UINT    Length;
	
public:

	HWND  Window;
};