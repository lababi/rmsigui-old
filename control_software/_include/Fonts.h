//***************************************************************************************
// RUTINAS PARA OBTENER FUENTES DEL SISTEMA
//***************************************************************************************

HFONT  GetIconFont();
HFONT  GetSystemFont(UINT, UINT bold);

enum {

	SYSFNT_CAPTION,
	SYSFNT_SMALL_CAPTION,
	SYSFNT_MENU,
	SYSFNT_STATUSBAR,
	SYSFNT_DIALOG
};
