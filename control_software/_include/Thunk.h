//***************************************************************************************
// DEFINICION DE LA CLASE CWndProcThunk
//***************************************************************************************

class CWndProcThunk {

#pragma pack(push, 1)

	struct SThunk {
		BYTE   _Mecx;
		DWORD  _This;
		BYTE   _Medx;
		DWORD  _MsgList;
		BYTE   _Meax;
		DWORD  _BaseProc;
		BYTE   _Jump;
		DWORD  _Proc;
	} _Thunk;

#pragma pack(pop)

public:

	 CWndProcThunk() {}
	~CWndProcThunk() {}

public:

	void Init(void *proc, void *p_this, void *p_msgInfo, void *baseProc = 0 ) {

		_Thunk._Mecx     = 0xB9;
		_Thunk._This     = reinterpret_cast<int>(p_this);
		_Thunk._Medx     = 0xBA;
		_Thunk._MsgList  = reinterpret_cast<int>(p_msgInfo);
		_Thunk._Meax     = 0xB8;
		_Thunk._BaseProc = reinterpret_cast<int>(baseProc);
		_Thunk._Jump     = 0xE9;
		_Thunk._Proc     = reinterpret_cast<int>(proc) - 
					       reinterpret_cast<int>(this) - sizeof(_Thunk);
	}
};
