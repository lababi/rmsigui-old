#include <windows.h>
#include <tchar.h>
#include <shlwapi.h>

#include "..\\_include\\CSerial.h"
#define MAX_KEY_LENGTH  255

// **************************************************************************************
//	AREA DE DATOS
// **************************************************************************************

TCHAR *CSerial::KeyName[] = {
//	_T( "SYSTEM\\CurrentControlSet\\Enum\\USB\\VID_1A86&PID_7523" ), // Arduino uno clon
	_T( "SYSTEM\\CurrentControlSet\\Enum\\USB\\VID_2341&PID_0043" ), // Arduino uno
	_T( "SYSTEM\\CurrentControlSet\\Enum\\USB\\VID_03EB&PID_204B" )  // ATmega 256
};

// **************************************************************************************
// IMPLEMENTACION DE LA CLASE CSERIAL
// **************************************************************************************

CSerial::~CSerial() {

	if( Port )
		Port = reinterpret_cast<CSerial *>( !CloseHandle( Port ) );

	OutputDebugString( _T("Puerto serial cerrado\n") );
}


CSerial* CSerial::OpenPort( TCHAR *PortName, UINT baudRate ) {

	// Constantes para el manejo de errores

	enum{ OPEN_PORT, CONFIGURATION };
	UINT   bresult = OPEN_PORT;

	// Establece comunicacion con el puerto
	switch( reinterpret_cast<UINT>( Port ) ) {

	default:
		return this;

	case 0:		// Abre el puerto

		Port = CreateFile( PortName, GENERIC_READ | GENERIC_WRITE, 0, 0, 
						   OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0 );

		if( Port == INVALID_HANDLE_VALUE )
			break;

		// Configuracion del puerto
		DCB dcb;
		ZeroMemory( &dcb, sizeof(dcb) );
		dcb.DCBlength = sizeof(dcb);
		bresult       = CONFIGURATION;

		if( GetCommState( Port, &dcb ) ) {
			dcb.BaudRate    = baudRate;
            dcb.ByteSize    = 8;
            dcb.StopBits    = ONESTOPBIT;
            dcb.Parity      = NOPARITY;
            dcb.fDtrControl = DTR_CONTROL_ENABLE;

			if ( !SetCommState( Port, &dcb ) ) 
				break;

			PurgeComm( Port, PURGE_RXCLEAR | PURGE_TXCLEAR );
		}

		// Set notifications
		SetCommMask( Port, EV_RXCHAR | EV_BREAK );

		// Set Timeouts
		COMMTIMEOUTS ctmo;
		ctmo.ReadIntervalTimeout         = MAXDWORD;
		ctmo.ReadTotalTimeoutMultiplier  = 0;
		ctmo.ReadTotalTimeoutConstant    = 0;
		ctmo.WriteTotalTimeoutMultiplier = 0;
		ctmo.WriteTotalTimeoutConstant   = 0;

		if( !SetCommTimeouts( Port, &ctmo ) )
			break;

		return this;
	}

	// Manejo de errores
	switch( bresult ) {

	case CONFIGURATION:
		CloseHandle( Port );

	case OPEN_PORT:
		Port = 0;

	default:
		return 0;
	}
}


void CSerial::EnumArduinoPorts( UINT port ) {

	UINT baudRate[] = { CBR_9600, CBR_115200 };

	// Abre el registro
	HKEY key;

	if( RegOpenKeyEx( HKEY_LOCAL_MACHINE, KeyName[ port ], 0, KEY_READ, &key )
	== ERROR_SUCCESS ) {

		QueryKey( key, baudRate[ port ] );
		RegCloseKey( key );
		return;
	}
}


void CSerial::QueryKey( HKEY hKey, UINT baudRate ) {

    TCHAR    achKey[ MAX_KEY_LENGTH ];       // buffer for subkey name
    DWORD    cbName;                         // size of name string 
    TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
    DWORD    cchClassName = MAX_PATH;        // size of class string 
    DWORD    cSubKeys = 0;                   // number of subkeys 
    DWORD    cbMaxSubKey;                    // longest subkey size 
    DWORD    cchMaxClass;                    // longest class string 
    DWORD    cValues;                        // number of values for key 
    DWORD    cchMaxValue;                    // longest value name 
    DWORD    cbMaxValueData;                 // longest value data 
    DWORD    cbSecurityDescriptor;           // size of security descriptor 
    FILETIME ftLastWriteTime;                // last write time 
 
    // Get the class name and the value count. 
    DWORD retCode = RegQueryInfoKey(
        hKey,                                // key handle 
        achClass,                            // buffer for class name 
        &cchClassName,                       // size of class string 
        NULL,                                // reserved 
        &cSubKeys,                           // number of subkeys 
        &cbMaxSubKey,                        // longest subkey size 
        &cchMaxClass,                        // longest class string 
        &cValues,                            // number of values for this key 
        &cchMaxValue,                        // longest value name 
        &cbMaxValueData,                     // longest value data 
        &cbSecurityDescriptor,               // security descriptor 
        &ftLastWriteTime );                  // last write time 
 
    // Enumerate the subkeys, until RegEnumKeyEx fails.    
    if( cSubKeys ) {

		TCHAR PortName[255];
		DWORD PortNameSize = 255 * sizeof( TCHAR );

        for ( UINT i = 0; i < cSubKeys; i++ ) { 

            cbName  = MAX_KEY_LENGTH;
            retCode = RegEnumKeyEx( hKey, i, achKey, &cbName, 
						  NULL, NULL, NULL, &ftLastWriteTime );

            if( retCode == ERROR_SUCCESS ) {

				// Formato de la direccion del registro			
				TCHAR *target = achKey + cbName;
				TCHAR *source = _T("\\Device Parameters");
				for( UINT i = 19; i --;  )
					*target ++ = *source ++;

				// Lectura del valor del registro
				RegGetValue( hKey, achKey, _T("PortName"), RRF_RT_ANY, 
					NULL, PortName, &PortNameSize );

				// Abre el puerto
				if( OpenPort( PortName, baudRate ) )
					return;
            }
        }
    }
}	


UINT CSerial::Send( char* string, UINT strLen ) {

	DWORD bytesWritten;
	return ( 0 != WriteFile( Port, string, strLen, &bytesWritten, 0 ) ) &
		   ( bytesWritten == strLen );
}


UINT CSerial::Read( char *storage, UINT size, CSerial::HANDLER handler ) {

	COMSTAT  status;
	DWORD    dwCommEvent, dwRead, dwError;
	UINT     commError, readError = -1, accum = 0;
	UINT     index = 0;

	BytesRead = 0;

	do {
		
		// Espera un evento de lectura 
//		SetCommMask( Port, EV_RXCHAR | EV_BREAK );
		WaitCommEvent( Port, &dwCommEvent, NULL );

		if( dwCommEvent & EV_BREAK )
			return 0;

		// Obtiene el numero de bytes en el buffer
		commError = ClearCommError( Port, &dwError, &status ) ? -1 : 0;

		// Reinicia el ciclo cuando el buffer esta vacio
		if( !status.cbInQue )
			continue;

		// Lectura del registro serial
		readError = ReadFile
					( Port, storage + accum, status.cbInQue, &dwRead, 0 ) ? -1 : 0;
		accum    += dwRead;
		
		// Rutina de procesamiento
		storage[ accum ] = 0;
		index            = ( *this.*handler ) ( storage, dwRead );
		BytesRead        = accum;

	} while( ( index == 0 ) & commError & readError );

	return commError & readError & ( accum - ( index != 0 ) );
}


// **************************************************************************************
// FUNCIONES DE PROCESAMIENTO DEL BUFFER SERIAL
//	  1) LAS FUNCIONES DEBEN REGRESAR UN INDICE BASE 1 / 0 EN CASO DE ERROR 
//    2) BytesRead ES ACTUALIZADO POR LA FUNCION "Read"
// **************************************************************************************
UINT CSerial::FindNewLineW( char *string, UINT strLen ) {

	char *target =  string + ( strLen + BytesRead + 1 ) * 2 - 1;
	UINT  index  = 0;
	string      += BytesRead + strLen;

	// Detecta 'Carriage Return' 0xA y convierte a WCHAR
	do {
		index      = ( index ^ strLen ) & ( 0 - ( *string != _T('\r') ) ) ^ strLen;
		*target -- = 0;
		*target -- = *string --;
	} while( strLen -- );

	index += ( index != 0 ? -1 : 0 ) & ( BytesRead + 1 );
	return index;
}


UINT CSerial::FindNewLine( char *string, UINT strLen ) {

	UINT  index  = 0;
	string      += BytesRead;

	// Detecta 'Carriage Return' 0xA
	while( ( *string ++ != '\n' ) & ( index < strLen ) )
		index ++;

	// 0 = error, 
	return ( index  + 1 ) & ( string[ -1 ] == '\n' ? -1 : 0 );
}


UINT CSerial::FindMessage( char *string, UINT strLen ) {

	static UINT match;

	char  *source, *target;
	UINT   symbol, index, parsed;
	UINT   offset[] = { 0, MessageLen - 1, 0, BytesRead };

	// Condiciones iniciales
	match   = ( BytesRead == 0 ? -1 : 0 ) | match;
	source  = Message + offset[ ( match & 2 ) | ( BytesRead != 0 ) ];
	target  = string + BytesRead;
	index   = BytesRead;
	strLen += BytesRead;

	// Busca el final de la linea y compara
	do {
		symbol = *target != '\n' ? -1 : 0;
		if( ~symbol | ( index >= strLen ) )
			break;

		match  &= *target ++ == *source;
		source +=  index ++ < MessageLen;

	} while( TRUE );

	// Regresa 0 = continue, 1 = no encontrado, > 1 = encontrado
	parsed = index == MessageLen ? -1 : 0;
	match  = match ? -1 : 0;

	return ~symbol & ( ( ~( match & parsed ) & 1 ) | 
		                  ( match & parsed & ( index + 1 ) ) );
}


// **************************************************************************************
// RUTINAS GENERICAS
// **************************************************************************************
UINT CSerial::LineCounting( char *string ) {

	UINT lines = 0;

	// Detecta 'Carriage Return' 0xa
	while( *string ) {
		lines += *string ++ == '\n';
	}

	return lines;
}