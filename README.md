# README #

RmsiGUI is a graphical user interface for analyzing mass spectrometry imaging (MSI) data with the statistical language R <https://www.r-project.org/>. 

### Status ###

Early development. For productive work, currently the command line version MSI.R is recommended: https://bitbucket.org/lababi/msi.r/.

### Installation ###

Required dependencies are installed during the first run.

### Running ###

1. Run the main script **``RmsiGUI.R``**,  
- Option 1: From the R command line: ``source('RmsiGUI.R')``.  
- Option 2: From an R IDE, such as RStudio <https://www.rstudio.com/> or RKWard <https://rkward.kde.org/>.  
2. Open the provided **RmsiGUI IP** in your favorite browser.

### Upload data ###

imzML files, have to be uploaded as zip file containing both, the imzML and ibd files. 

### Contribution guidelines ###

Please report your experiences, feature requests and problems.

### License ###

GNU General Public License, version 3 (http://gplv3.fsf.org/).

### Contact ###

robert.winkler@cinvestav.mx, robert.winkler.mail@gmail.com